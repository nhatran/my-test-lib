
import json
import os
import shutil
from update_patch_version import increase_version, git_commit_rn_components, git_commit_native

from os import listdir
import os.path
from os.path import isfile, join
import shutil


from pick import pick
from zipfile import ZipFile


def write_output(file_path, data):
    print('Updating %s' % file_path)
    with open(file_path, 'w') as outfile:
        json.dump(data, outfile, indent=4)


def parseSelectionJson(input_path, export_path):
    allIcon = {}
    print('parseSelectionJson %s' % input_path)
    with open(input_path) as json_file:
        json_data = json.load(json_file)
        icons = json_data['icons']

    for oneIcon in icons:
        name = oneIcon['properties']['name']
        code = oneIcon['properties']['code']
        colors = oneIcon['attrs']
        jsonIcon = {}
        jsonIcon['rawcode'] = code
        jsonIcon['colors'] = colors
        jsonIcon['code'] = chr(code)
        if 'codes' in oneIcon['properties']:
            jsonIcon['codes'] = oneIcon['properties']['codes']
        allIcon[name] = jsonIcon
    write_output(export_path, allIcon)


def findFileWithExtension(folder_path, extension):
    return [f for f in listdir(folder_path) if isfile(join(folder_path, f)) and f.endswith(extension)]

def parseIcon():
  zipfiles = findFileWithExtension('./', 'zip')
  if len(zipfiles) == 1:
    zipfile = zipfiles[0]
    print('Parse icon with file: %s' % zipfile)
  elif len(zipfiles) > 1:
    zipfile, index = pick(zipfiles, 'Please choose zip file: ')
  else:
    print('Not found zip file')
    exit()
  zip_name, extension = os.path.splitext(zipfile)
  with ZipFile(zipfile) as zf:
    zf.extractall(zip_name)
  
  font_name, extension = os.path.splitext(
    findFileWithExtension(join(zip_name, 'fonts'), 'ttf')[0])
  if os.path.exists(font_name):
    shutil.rmtree(font_name)
  os.makedirs(font_name)
  parseSelectionJson(join(zip_name, 'selection.json'), join(font_name, font_name + '.json'))
  zip_name_path = join(join(zip_name, 'fonts'), font_name + '.ttf')
  shutil.copy(zip_name_path, font_name) 
  shutil.rmtree(zip_name)
  return (zip_name, font_name)

def copy_all_folder(resource, target): 
  for file_name in os.listdir(resource):
    full_file_name = os.path.join(resource, file_name)
    if (os.path.isfile(full_file_name)):
        shutil.copy(full_file_name, target)

(zip_name, font_name) = parseIcon()
if not font_name == 'zalopay':
  exit()

increase_version()
zalopay_ios = os.environ.get('ZALOPAY_IOS')
zalopay_android = os.environ.get('ZALOPAY_ANDROID')

lib_target = '../src/lib/fonts'
copy_all_folder(font_name, lib_target)

git_commit_rn_components(zip_name, join(lib_target, font_name))
root_path = os.getcwd();

if zalopay_ios:
  inner_ios = 'ZaloPay/Resources/Fonts'
  zalopay_ios_full_path = join(zalopay_ios, inner_ios)
  copy_all_folder(font_name, zalopay_ios_full_path)
  os.chdir(zalopay_ios_full_path)
  git_commit_native(zip_name, font_name)

os.chdir(root_path)

if zalopay_android:
  inner_android = 'ZaloPay/app/src/main/assets/fonts'
  zalopay_android_full_path = join(zalopay_android, inner_android)
  copy_all_folder(font_name, zalopay_android_full_path)
  os.chdir(zalopay_android_full_path)
  git_commit_native(zip_name, font_name)

os.chdir(root_path)
shutil.rmtree(font_name)

    

