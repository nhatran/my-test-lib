#!/usr/local/bin/python3

import subprocess
from os.path import join
import json
import io


def read_json_file(file_path):
    with open(file_path) as json_data:
        d = json.load(json_data)
        return d


def write_json_file(data, file_path):
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    with io.open(file_path, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(data, indent=2, sort_keys=False,
                          separators=(',', ': '), ensure_ascii=False)
        outfile.write(to_unicode(str_))
    return


def get_new_version(oldVersion):
    arr = oldVersion.split('.')
    patch_version = str(int(arr[2]) + 1)
    arr[2] = patch_version
    newversion = '.'.join(arr)
    return newversion


def increase_version():
    file_path = '../package.json'
    package_json = read_json_file(file_path)
    version = package_json['version']
    newversion = get_new_version(version)
    package_json['version'] = newversion
    write_json_file(package_json, file_path)
    return


def git_commit_rn_components(zip_file, font_name):
    subprocess.run(["git", "add"] + ["../package.json"])
    subprocess.run(["git", "add"] + [font_name + '.ttf'])
    subprocess.run(["git", "add"] + [font_name + '.json'])
    message = '[Font] Update icon font %s' % (zip_file)
    subprocess.run(["git", "commit", "-m", message])
    return

def git_commit_native(zip_file, font_name):
    subprocess.run(["git", "add"] + [font_name + '.ttf'])
    subprocess.run(["git", "add"] + [font_name + '.json'])
    message = '[Font] Update icon font %s' % (zip_file)
    subprocess.run(["git", "commit", "-m", message])
    return
