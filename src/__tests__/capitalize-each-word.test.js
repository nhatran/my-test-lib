import Utils from '../lib/utils';

const { capitalizeEachWord } = Utils;

describe('test suite function capitalizeEachWord', () => {
  test('test capitalizeEachWord', () => {
    expect(capitalizeEachWord('Cường Đô la sáu múi')).toEqual('Cường Đô La Sáu Múi');
    expect(capitalizeEachWord('CƯỜNG ĐÔ LA SÁU MÚI')).toEqual('Cường Đô La Sáu Múi');
    expect(capitalizeEachWord('CƯỜNG ĐÔ LA Sáu MÚI')).toEqual('Cường Đô La Sáu Múi');
    expect(capitalizeEachWord('Cường Đô la sáu múi')).toEqual('Cường Đô La Sáu Múi');
    expect(capitalizeEachWord('12/476 Hoà hảo, p7 quận 10')).toEqual('12/476 Hoà Hảo, P7 Quận 10');
    expect(capitalizeEachWord('12/476 HOÀ HẢO, P7 QUẬN 10')).toEqual('12/476 Hoà Hảo, P7 Quận 10');
    expect(capitalizeEachWord(' =]] :) & @ # % ^ & * ~ - + [ ] ; < > ? ` HOÀ HẢO')).toEqual(' =]] :) & @ # % ^ & * ~ - + [ ] ; < > ? ` Hoà Hảo');
    expect(capitalizeEachWord('😀 🏴‍☠️  🦊 🦊 QUẬN 10')).toEqual('😀 🏴‍☠️  🦊 🦊 Quận 10');
    expect(capitalizeEachWord('🧛 🧛 🧛 QUẬN 10')).toEqual('🧛 🧛 🧛 Quận 10');
  });
});
