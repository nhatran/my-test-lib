import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';

import { Bill } from '../lib/detail-component/cps-detail';
import { Strings } from '../lib/constants';

const { describe, it } = global;
const ServiceType = Strings.ServiceType;

const billDatas = {
  supplierCode: 'Điện HN',
  customerCode: 'PD01010',
  status: 1,
  transId: '171010-0000006',
  transactionDate: '2017-10-10 09:38:17.0',
  zptransId: 171010000000018,
  item:  [ { i: '449857', m: '02/2017', a: '20000' } ],
  unitPrice: 20000,
  discountamount: 1000
};

describe('billUnitTest', () => {
  it('parseDataInBill', () => {
    const wrapper = shallow(<Bill {...billDatas} amount={billDatas.unitPrice} apptransid={billDatas.transId} zptransid={billDatas.zptransId} detailType={ServiceType.ELECTRIC} />); 
    const datas = wrapper.instance().parseData();
    expect(datas).toEqual([ 
      { title: 'Dịch vụ', desc: 'Thanh toán tiền điện' },
      { title: 'Nhà cung cấp', desc: 'Điện HN' },
      { title: 'Mã khách hàng', desc: 'PD01010' },
      { title: 'Giá ban đầu', amount: 20000 },
      { title: 'Quà tặng', amount: -1000 },
      { title: 'Trạng thái', desc: 'Thành công' },
      { title: 'Thời gian', desc: '10/10/2017 09:38' },
      { title: 'Mã đơn hàng', desc: '171010-0000006' },
      { title: 'Mã giao dịch', desc: '171010-000000018' }
    ]);
  });
});
