
import Utils from '../lib/utils';
const {
  zpParserItem
} = Utils;

describe('test function zpParserItem', () => {
  test('zpParserItem', () => {
    expect(
      zpParserItem(
        '{"ext":"Người nhận:Phucpv\\tSố điện thoại:094 9538345","transtype":4}'
      )
    ).toEqual([
      { title: 'Người nhận', desc: 'Phucpv' },
      { title: 'Số điện thoại', desc: '094 9538345' }
    ]);
    expect(
      zpParserItem(
        '{"ext":"Người nhận:🦀:🦀\\tSố điện thoại:094 9538345","transtype":4}'
      )
    ).toEqual([
      { title: 'Người nhận', desc: '🦀:🦀' },
      { title: 'Số điện thoại', desc: '094 9538345' }
    ]);
    expect(zpParserItem('{"ext":"Người nhận}')).toEqual([]);
    expect(
      zpParserItem(
        '{"abc":"Người nhận:🦀\\tSố điện thoại:094 9538345","transtype":4}'
      )
    ).toEqual([]);
    expect(
      zpParserItem('{"ext":"Người nhận:🦀\\tabc:%^&^^#&@*@*:*@\\tMã giao dịch:123456789","transtype":4}')
    ).toEqual([
      { title: 'Người nhận', desc: '🦀' },
      { title: 'abc', desc: '%^&^^#&@*@*:*@' },
      { title: 'Mã giao dịch', desc: '123456789'}
    ]);
    expect(
      zpParserItem('sadkfjl unit test unit test :D')
    ).toEqual([]);
    expect(
      zpParserItem('{"ext":"sadfkjlkjqwlkejrljdsakfj"')
    ).toEqual([]);
    expect(
      zpParserItem('{"transtype":4}')
    ).toEqual([]);
  });

  test('zpParserItem test suite 2', () => {
    expect(zpParserItem('{"ext":"Tên KH:Nguyễn Danh Khôi\\tĐịa chỉ:268 Lý Thường Kiệt, quận 10, HCM"}')).toEqual(
      [
        { title: 'Tên KH', desc: 'Nguyễn Danh Khôi' },
        { title: 'Địa chỉ', desc: '268 Lý Thường Kiệt, quận 10, HCM' }
      ]
    );
    expect(zpParserItem('{"ext":"Tên KH:\\tĐịa chỉ:268 Lý Thường Kiệt, quận 10, HCM"}')).toEqual([
      { title: 'Địa chỉ', desc: '268 Lý Thường Kiệt, quận 10, HCM' }
    ]);
    expect(zpParserItem('{"ext":"Tên KH:Nguyễn Danh Khôi\\tĐịa chỉ:"}')).toEqual([
      { title: 'Tên KH', desc: 'Nguyễn Danh Khôi' }
    ]);
    expect(zpParserItem('{"ext":"Tên KH:\\tĐịa chỉ:"}')).toEqual([]);
  });
});
