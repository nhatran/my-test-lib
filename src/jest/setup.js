import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

global.__DEV__ = true;

configure({ adapter: new Adapter() });