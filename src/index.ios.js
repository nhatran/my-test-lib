/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { AppRegistry, Text, View, StyleSheet, Image } from 'react-native';
import { ZPNavigator, ZPDetailComponent, Colors, NavBar, Utils, console } from './lib';

Text.defaultProps.allowFontScaling = false;

const transaction = {
  description: 'Dcxdgy',
  appid: 1,
  userchargeamt: 10000,
  amount: 10000,
  userid: '170823000003001',
  // userfeeamt: 200,
  item: '{"ext":"Người nhận:Mr Bư\\tSố điện thoại:094 9538345","transtype":4}',
  type: 4,
  transid: 170907000000220,
  platform: 'ios',
  appusername: 'Diệp Tâm',
  pmcid: 38,
  username: 'Mr Bư',
  thankmessage: '',
  statustype: 1,
  reqdate: 1504767045147,
  sign: 1,
  discountamount: 1000,
};


class DetailComponent extends React.Component {
  render() {
    return (
      <ZPDetailComponent
        style={styles.main}
        {...transaction}
        timestamp={transaction.reqdate}
        zptransid={transaction.transid}
        transType='Chuyển tiền'
      />
    );
  }
}

const {total} = Utils.NavigatorBarHeight;

const Main = () => <RNNavigator component={DetailComponent} title="zalopay-component" />;
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    // bottom: 0,
    backgroundColor: 'red'
  },
  image: {
    position: 'absolute',
    left: 10,
    bottom: 0
  },
  main: {
    marginTop: total
  }

});
class RNNavigator extends ZPNavigator {
  renderScene(route, nav) {
    this.navigator = nav;
    this.route = route;
    console.log('renderScene');
    route.navBackgroundColor = Colors.transparent;
    const onLeftButtonPress =
      route.onLeftButtonPress || this.handleLeftButtonPress;

    return (
      <View style={styles.container}>
        <route.component navigator={nav}  {...route.passProps} style={styles.main} />
        
        <NavBar
          {...route}
          containerStyle={styles.containerStyle}
          onLeftButtonPress={onLeftButtonPress}
          centerTitle={true}
        > 
          {<Image style={styles.image} source={require('./images/all_bgnavigation70.jpg')} />}
        </NavBar>
        
      </View>
    );
  }
}

AppRegistry.registerComponent('example', () => Main);
