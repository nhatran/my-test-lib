import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StatusBar, Text, View, Platform, StyleSheet } from 'react-native';

import { Colors, Fonts } from '../themes';

import NavbarButton from './NavbarButton';

const NAV_BAR_HEIGHT = 44;
const STATUS_BAR_HEIGHT = 20;

const styles = StyleSheet.create({
  navBarContainer: {
    backgroundColor: Colors.white
  },
  statusBar: {
    height: STATUS_BAR_HEIGHT
  },
  navBar: {
    height: NAV_BAR_HEIGHT,
    flexDirection: 'row'
  },
  customTitle: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 7,
    alignItems: 'center'
  },
  navBarTitleContainer: {
    justifyContent: 'center'
  },
  navBarTitleText: {
    fontSize: Fonts.navigationSize,
    color: Colors.defaultText
  }
});

const ButtonShape = {
  title: PropTypes.string.isRequired,
  style: PropTypes.number,
  handler: PropTypes.func,
  disabled: PropTypes.bool
};

const TitleShape = {
  title: PropTypes.string.isRequired,
  tintColor: PropTypes.string
};

const StatusBarShape = {
  style: PropTypes.oneOf(['light-content', 'default']),
  hidden: PropTypes.bool,
  tintColor: PropTypes.string,
  hideAnimation: PropTypes.oneOf(['fade', 'slide', 'none']),
  showAnimation: PropTypes.oneOf(['fade', 'slide', 'none'])
};

function getButtonElement(data) {
  return (
    <View>
      {!data || data.props ? data : <NavbarButton title={data.title} tintColor={data.tintColor} handler={data.handler} accessible={data.accessible} accessibilityLabel={data.accessibilityLabel} />}
    </View>
  );
}

function getTitleElement(data) {
  if (!data || data.props) {
    return <View style={styles.customTitle}>{data}</View>;
  }

  const colorStyle = data.tintColor ? { color: data.tintColor } : null;

  return (
    <View style={[styles.navBarTitleContainer, data.containerStyle]}>
      <Text style={[styles.navBarTitleText, data.style, colorStyle]}>{data.title}</Text>
    </View>
  );
}

export default class NavigationBar extends Component {
  static propTypes = {
    style: View.propTypes.style,
    tintColor: PropTypes.string,
    statusBar: PropTypes.shape(StatusBarShape),
    leftButton: PropTypes.oneOfType([PropTypes.shape(ButtonShape), PropTypes.element, PropTypes.oneOf([null])]),
    rightButton: PropTypes.oneOfType([PropTypes.shape(ButtonShape), PropTypes.element, PropTypes.oneOf([null])]),
    title: PropTypes.oneOfType([PropTypes.shape(TitleShape), PropTypes.element, PropTypes.oneOf([null])]),
    containerStyle: View.propTypes.style
  };

  static defaultProps = {
    style: {},
    tintColor: '',
    leftButton: null,
    rightButton: null,
    title: null,
    statusBar: {
      style: 'default',
      hidden: false,
      hideAnimation: 'slide',
      showAnimation: 'slide'
    },
    containerStyle: {}
  };

  componentDidMount() {
    // this.customizeStatusBar();
  }

  componentWillReceiveProps() {
    // this.customizeStatusBar();
  }

  customizeStatusBar() {
    const { statusBar } = this.props;
    if (Platform.OS === 'ios') {
      if (statusBar.style) {
        StatusBar.setBarStyle(statusBar.style);
      }

      const animation = statusBar.hidden ? statusBar.hideAnimation : statusBar.showAnimation;

      StatusBar.showHideTransition = animation;
      StatusBar.hidden = statusBar.hidden;
    }
  }

  renderNavBar() {
    const { title, leftButton, rightButton, style } = this.props;
    return (
      <View style={[styles.navBar, style]}>
        {getButtonElement(leftButton)}
        {getTitleElement(title)}
        {getButtonElement(rightButton)}
      </View>
    );
  }

  render() {
    const { containerStyle, tintColor } = this.props;
    const customTintColor = tintColor ? { backgroundColor: tintColor } : null;

    const customStatusBarTintColor = this.props.statusBar.tintColor ? { backgroundColor: this.props.statusBar.tintColor } : null;

    let statusBar = null;

    if (Platform.OS === 'ios') {
      statusBar = !this.props.statusBar.hidden ? <View style={[styles.statusBar, customStatusBarTintColor]} /> : null;
    }

    return (
      <View style={[styles.navBarContainer, containerStyle, customTintColor]}>
        {statusBar}
        {this.renderNavBar()}
      </View>
    );
  }
}
