import React from 'react';
import PropTypes from 'prop-types';
import { SectionList } from 'react-native';
import SwipeListView from './SwipeListView';

import SwipeRow from './swipe-row';

class CustomSwipeListView extends SwipeListView {
  constructor(props) {
    super(props);
    this._keyExtractor = this._keyExtractor.bind(this);
  }

  _keyExtractor(item, index) {
    if (this.props.keyExtractor) {
      return this.props.keyExtractor(item, index);
    }
    return index;
  }

  onRowOpen(rowId, rowMap) {
    const cellIdentifier = rowId;
    if (this.openCellId && this.openCellId !== cellIdentifier) {
      this.safeCloseOpenRow();
    }
    this.openCellId = cellIdentifier;
    this.props.onRowOpen && this.props.onRowOpen(rowId, rowMap);
  }

  renderItem(info, rowMap) {
    const rowId = info.index;
    const rowData = info.item;
    const sectionTitle = info.section.title;
    const id = `${sectionTitle}-${rowId}`;
    if (!this.props.renderHiddenRow) {
      const Component = this.props.renderItem(info, rowMap);
      return React.cloneElement(Component, {
        ...Component.props,
        ref: row => (this._rows[id] = row),
        onRowOpen: () => this.onRowOpen(id, this._rows),
        onRowDidOpen: () =>
          this.props.onRowDidOpen && this.props.onRowDidOpen(id, this._rows),
        onRowClose: () =>
          this.props.onRowClose && this.props.onRowClose(id, this._rows),
        onRowDidClose: () =>
          this.props.onRowDidClose && this.props.onRowDidClose(id, this._rows),
        onRowPress: () => this.onRowPress(),
        //setScrollEnabled: enable => this.setScrollEnabled(enable),
        swipeGestureBegan: () => this.rowSwipeGestureBegan(id)
      });
    }
    return (
      <SwipeRow
        ref={row => (this._rows[id] = row)}
        swipeGestureBegan={() => this.rowSwipeGestureBegan(id)}
        onRowOpen={() => this.onRowOpen(id, this._rows)}
        onRowDidOpen={() =>
          this.props.onRowDidOpen && this.props.onRowDidOpen(id, this._rows)}
        onRowClose={() =>
          this.props.onRowClose && this.props.onRowClose(id, this._rows)}
        onRowDidClose={() =>
          this.props.onRowDidClose && this.props.onRowDidClose(id, this._rows)}
        onRowPress={() => this.onRowPress(id)}
        //setScrollEnabled={enable => this.setScrollEnabled(enable)}
        leftOpenValue={this.props.leftOpenValue}
        rightOpenValue={this.props.rightOpenValue}
        closeOnRowPress={this.props.closeOnRowPress}
        disableLeftSwipe={this.props.disableLeftSwipe}
        disableRightSwipe={this.props.disableRightSwipe}
        stopLeftSwipe={this.props.stopLeftSwipe}
        stopRightSwipe={this.props.stopRightSwipe}
        recalculateHiddenLayout={this.props.recalculateHiddenLayout}
        style={this.props.swipeRowStyle}
        preview={this.props.previewFirstRow || this.props.previewRowIndex}
        previewDuration={this.props.previewDuration}
        previewOpenValue={this.props.previewOpenValue}
        tension={this.props.tension}
        friction={this.props.friction}
        directionalDistanceChangeThreshold={
          this.props.directionalDistanceChangeThreshold
        }
        swipeToOpenPercent={this.props.swipeToOpenPercent}
      >
        {this.props.renderHiddenRow(rowData, id, this._rows)}
        {this.props.renderItem(info)}
      </SwipeRow>
    );
  }

  render() {
    return (
      <SectionList
        {...this.props}
        ref={c => this.setRefs(c)}
        keyExtractor={this._keyExtractor}
        onScroll={e => this.onScroll(e)}
        renderItem={info => this.renderItem(info, this._rows)}
      />
    );
  }
}

CustomSwipeListView.propTypes = {
  renderItem: PropTypes.func.isRequired
};

CustomSwipeListView.defaultProps = {
  renderRow: () => {}
};

export default CustomSwipeListView;
