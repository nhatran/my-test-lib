import React from 'react';
import { TouchableOpacity } from 'react-native';
import SwipeRow from './SwipeRow';

class CustomSwipeRow extends SwipeRow {
  renderVisibleContent() {
    const onPress = this.props.children[1].props.onPress;
    if (!onPress) {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => this.onRowPress()}>
          {this.props.children[1]}
        </TouchableOpacity>
      );
    }

    const newOnPress = () => {
      this.onRowPress();
      setTimeout(() => {
        onPress();
      }, 200);
    };
    return React.cloneElement(this.props.children[1], {
      ...this.props.children[1].props,
      onPress: newOnPress
    });
  }
}

export default CustomSwipeRow;
