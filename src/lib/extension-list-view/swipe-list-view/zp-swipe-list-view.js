import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

import { Fonts, Colors } from '../../themes';
import ZPIcon from '../../icomoon';
import SwipeListView from './swipe-list-view';
import MoreButton from '../more-button';

const deleteWidth = 75;
const swipeStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  button: {
    width: deleteWidth,
    flex: 1,
    backgroundColor: Colors.error,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: Colors.white,
    fontFamily: Fonts.regular,
    fontSize: Fonts.buttonFontSize,
    marginTop: 5
  },
  touchable: {
    flex: 1
  }
});

export default class ZPSwipeListView extends React.Component {
  constructor(props) {
    super(props);
    this.handleLoadMorePress = this.handleLoadMorePress.bind(this);
    this.getRowCount = this.getRowCount.bind(this);

    this.renderRemoveButton = this.renderRemoveButton.bind(this);
    this.renderHiddenRow = this.renderHiddenRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  getRowCount() {
    let count = 0;
    this.props.sections.forEach(element => {
      count += element.data.length;
    });
    return count;
  }

  scrollToTop(animated, params = {}) {
    this.listViewRef.scrollToLocation &&
      this.listViewRef.scrollToLocation({
        itemIndex: 0,
        sectionIndex: 0,
        viewPosition: 0,
        viewOffset: 50, // viewOffset is a fixed number of pixels to offset the final target position, e.g. to compensate for sticky headers.
        ...params,
        animated,
      });
  }

  handleRemove(data) {
    this.listView.safeCloseOpenRow();
    setTimeout(() => {
      this.props.onRemove && this.props.onRemove(data);
    }, 250);
  }

  handleLoadMorePress() {
    this.props.onMoreButtonPress && this.props.onMoreButtonPress();
  }

  renderRemoveButton(data) {
    return (
      <View style={[swipeStyles.container, { backgroundColor: this.props.rowColor }]}>
        <TouchableOpacity
          style={swipeStyles.touchable}
          onPress={() => {
            this.handleRemove(data);
          }}
        >
          <View style={swipeStyles.button}>
            {<ZPIcon name={'general_delete_card'} size={18} />}
            <Text style={swipeStyles.title}>{this.props.buttonTitle}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  renderHiddenRow(rowData) {
    if (this.props.renderHiddenRow) {
      return this.props.renderHiddenRow;
    }
    return this.renderRemoveButton(rowData);
  }

  renderFooter() {
    return <MoreButton showLoadMore={this.props.showLoadMore} onPress={this.handleLoadMorePress} />;
  }

  render() {
    return (
      <SwipeListView
        ref={listView => (this.listView = listView)}
        listViewRef={listViewRef => (this.listViewRef = listViewRef)}
        ListFooterComponent={this.renderFooter}
        renderHiddenRow={this.renderHiddenRow}
        rightOpenValue={-deleteWidth}
        disableRightSwipe={true}
        closeOnRowPress={true}
        recalculateHiddenLayout={true}
        {...this.props}
      />
    );
  }
}

ZPSwipeListView.propTypes = {
  onMoreButtonPress: PropTypes.func,
  onRemove: PropTypes.func,
  showLoadMore: PropTypes.bool,
  buttonTitle: PropTypes.string,
  renderHiddenRow: PropTypes.func,
  rowColor: PropTypes.string
};

ZPSwipeListView.defaultProps = {
  showLoadMore: false,
  buttonTitle: 'Xoá'
};
