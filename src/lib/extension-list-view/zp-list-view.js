import React from 'react';
import PropTypes from 'prop-types';
import { SectionList } from 'react-native';

import MoreButton from './more-button';

export default class ExListView extends React.Component {
  constructor(props) {
    super(props);

    this._handleLoadMorePress = this._handleLoadMorePress.bind(this);
    this._keyExtractor = this._keyExtractor.bind(this);

    this._renderFooter = this._renderFooter.bind(this);
  }

  _keyExtractor(item, index) {
    if (this.props.keyExtractor) {
      return this.props.keyExtractor;
    }
    return index;
  }

  _handleLoadMorePress() {
    this.props.onMoreButtonPress && this.props.onMoreButtonPress();
  }

  _renderFooter() {
    return <MoreButton showLoadMore={this.props.showLoadMore} onPress={this._handleLoadMorePress} />;
  }

  render() {
    return <SectionList {...this.props} keyExtractor={this._keyExtractor} ListFooterComponent={this._renderFooter} />;
  }
}

ExListView.propTypes = {
  onMoreButtonPress: PropTypes.func,
  showLoadMore: PropTypes.bool
};

ExListView.defaultProps = {
  showLoadMore: false
};
