import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Strings } from '../constants';
import { Fonts, Colors } from '../themes';
import ZPIcon from '../icomoon';

const styles = StyleSheet.create({
  buttonBackgroundColor: {
    backgroundColor: Colors.transparent,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    fontFamily: Fonts.regular,
    fontSize: Fonts.noteSize,
    color: Colors.zalo,
    marginRight: 10,
  },
  icon: {
    transform: [{ rotate: '90deg' }],
  },
});

const MoreButton = props => {
  if (props.showLoadMore) {
    return (
      <TouchableOpacity style={styles.loadMore} onPress={props.onPress}>
        <View style={styles.buttonBackgroundColor}>
          <Text allowFontScaling={false} style={styles.text}>
            {Strings.SEE_MORE}
          </Text>
          <ZPIcon style={styles.icon} name={'general_arrowright'} size={15} color={Colors.zalo} />
        </View>
      </TouchableOpacity>
    );
  }
  return <View />;
};

MoreButton.propTypes = {
  onPress: PropTypes.func,
  showLoadMore: PropTypes.bool,
};
export default MoreButton;
