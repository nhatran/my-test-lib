import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  BackHandler,
  StatusBar,
  StyleSheet,
  Platform
} from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';

import { getZaloPayApi } from '../generate';

import NavBar from './exnavbar';

StatusBar.setBarStyle('light-content');

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

class ZPNavigator extends React.Component {
  constructor(props) {
    super(props);
    this.handleLeftButtonPress = this.handleLeftButtonPress.bind(this);
    this._handleHardwareBackPress = this._handleHardwareBackPress.bind(this);
    this.renderScene = this.renderScene.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener(
        'hardwareBackPress',
        this._handleHardwareBackPress
      );
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this._handleHardwareBackPress
      );
    }
  }

  _handleHardwareBackPress() {
    if (!this.navigator) {
      return false;
    }
    if (this.route.onLeftButtonPress) {
      this.route.onLeftButtonPress();
      return true;
    }
    if (this.navigator.getCurrentRoutes().length === 1) {
      return false;
    }
    this.navigator.pop();
    return true;
  }

  handleLeftButtonPress() {
    if (this.route.onLeftButtonPress) {
      this.route.onLeftButtonPress();
    } else {
      this.goBack();
    }
  }

  goBack() {
    if (this.navigator.getCurrentRoutes().length > 1) {
      this.navigator.pop();
    } else {
      const ZaloPayApi = getZaloPayApi();
      ZaloPayApi && ZaloPayApi.closeModule(this.props.moduleId);
    }
  }

  renderScene(route, nav) {
    this.navigator = nav;
    this.route = route;
    return (
      <View style={styles.container}>
        <NavBar
          {...route}
          onLeftButtonPress={
            route.onLeftButtonPress || this.handleLeftButtonPress
          }
          centerTitle={Platform.OS === 'ios'}
        />
        <route.component navigator={nav} {...route.passProps} />
      </View>
    );
  }

  render() {
    return (
      <Navigator
        showNavigationBar={false}
        {...this.props}
        initialRoute={{
          passProps: {
            ...this.props.passProps
          },
          ...this.props
        }}
        style={styles.container}
        renderScene={this.renderScene}
        configureScene={route => ({
          ...(route.sceneConfig || Navigator.SceneConfigs.PushFromRight),
          gestures: route.gestures
        })}
      />
    );
  }
}

ZPNavigator.propTypes = {
  zalopay_userid: PropTypes.string,
  onRightButtonPress: PropTypes.func,
  title: PropTypes.string
};

export default ZPNavigator;
