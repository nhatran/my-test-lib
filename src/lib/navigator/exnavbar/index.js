import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Platform, TouchableOpacity } from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';
import DismissKeyboard from 'react-native-dismiss-keyboard';

import NavigationBar from '../../react-native-navigation-bar';

import { Colors, Fonts, Utils, ZPIcon } from '../../generate';
import { DismissKeyboardView } from '../../common';

const isIphoneX = Utils.isIphoneX();
const STATUS_BAR_HEIGHT = isIphoneX
  ? 44
  : Navigator.NavigationBar.Styles.General.StatusBarHeight;
const height =
  STATUS_BAR_HEIGHT + Navigator.NavigationBar.Styles.General.NavBarHeight;

const styles = StyleSheet.create({
  navBar: {
    justifyContent: isIphoneX ? 'flex-end' : 'center',
    height
  },
  button: {
    width: 44,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center'
  },
  /* eslint-disable react-native/no-unused-styles */
  title: {
    color: Colors.white,
    fontFamily: Fonts.semibold,
    fontSize: Fonts.navigationSize
  },
  titleContainer: {
    flex: 1,
    marginLeft: 26
  },
  navContainer: {
    justifyContent: 'space-between'
  }
  /* eslint-enable react-native/no-unused-styles */
});

const backIcon =
  Platform.OS === 'ios' ? 'general_backios' : 'general_backandroid';

class NavBarIOS extends React.Component {
  constructor(props) {
    super(props);
    this.handleLeftButtonPress = this.handleLeftButtonPress.bind(this);
    this.handleRightButtonPress = this.handleRightButtonPress.bind(this);
  }
  handleLeftButtonPress() {
    this.props.onLeftButtonPress && this.props.onLeftButtonPress();
    DismissKeyboard();
  }
  handleRightButtonPress() {
    this.props.onRightButtonPress && this.props.onRightButtonPress();
  }
  renderRightButton() {
    if (this.props.renderRightButton) {
      return this.props.renderRightButton();
    }
    if (this.props.rightIcon) {
      return (
        <TouchableOpacity
          onPress={this.handleRightButtonPress}
          style={styles.button}
        >
          <ZPIcon
            name={this.props.rightIcon}
            size={this.props.rightIconSize}
            color={Colors.white}
          />
        </TouchableOpacity>
      );
    }
    return <TouchableOpacity style={styles.button} />;
  }
  renderLeftButton() {
    if (this.props.renderLeftButton) {
      return this.props.renderLeftButton();
    }
    if (!this.props.hiddenLeftButton) {
      return (
        <TouchableOpacity
          onPress={this.handleLeftButtonPress}
          style={styles.button}
        >
          <ZPIcon name={backIcon} size={20} color={Colors.white} />
        </TouchableOpacity>
      );
    }
    return <TouchableOpacity style={styles.button} />;
  }
  render() {
    const { navBackgroundColor, centerTitle, containerStyle } = this.props;

    const navStyle = centerTitle ? styles.navContainer : null;

    const titleConfig = {
      containerStyle: centerTitle ? null : styles.titleContainer,
      title: this.props.title,
      style: styles.title
    };

    return (
      <DismissKeyboardView style={containerStyle}>
        {this.props.children}
        <NavigationBar
          containerStyle={styles.navBar}
          style={navStyle}
          tintColor={navBackgroundColor}
          leftButton={this.renderLeftButton()}
          rightButton={this.renderRightButton()}
          title={titleConfig}
        />
      </DismissKeyboardView>
    );
  }
}

NavBarIOS.propTypes = {
  ...NavigationBar.propTypes,
  renderLeftButton: PropTypes.func,
  renderRightButton: PropTypes.func,
  onLeftButtonPress: PropTypes.func,
  onRightButtonPress: PropTypes.func,
  title: PropTypes.string,
  navBackgroundColor: PropTypes.string,
  rightIcon: PropTypes.string,
  hiddenLeftButton: PropTypes.bool,
  rightIconSize: PropTypes.number,
  centerTitle: PropTypes.bool
};

NavBarIOS.defaultProps = {
  navBackgroundColor: Colors.zalo,
  hiddenLeftButton: false,
  rightIconSize: 20
};
export default NavBarIOS;
