/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule Modal
 * @flow
 */
'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  StyleSheet,
  View,
  requireNativeComponent
} from 'react-native';

const RCTModalHostView = requireNativeComponent('ZPModalHostView', null);

const styles = StyleSheet.create({
  modal: {
    position: 'absolute'
  },
  container: {
    position: 'absolute',
    left: 0,
    top: 0
  }
});

class Modal extends React.Component {
  _shouldSetResponder = function() {
    return true;
  };

  render() {
    if (this.props.visible === false) {
      return null;
    }

    const containerStyles = {
      backgroundColor: this.props.transparent ? 'transparent' : 'white',
      top: Platform.OS === 'android' && 0 //Platform.Version >= 19 ? UIManager.RCTModalHostView.zp-constants.StatusBarHeight : 0,
    };

    let animationType = this.props.animationType;
    if (!animationType) {
      // manually setting default prop here to keep support for the deprecated 'animated' prop
      animationType = 'none';
      if (this.props.animated) {
        animationType = 'slide';
      }
    }

    return (
      <RCTModalHostView
        animationType={animationType}
        transparent={this.props.transparent}
        onRequestClose={this.props.onRequestClose}
        onShow={this.props.onShow}
        style={styles.modal}
        onStartShouldSetResponder={this._shouldSetResponder}
      >
        <View style={[styles.container, containerStyles]}>
          {this.props.children}
        </View>
      </RCTModalHostView>
    );
  }
}

Modal.propTypes = {
  animated: PropTypes.bool,
  animationType: PropTypes.oneOf(['none', 'slide', 'fade']),
  transparent: PropTypes.bool,
  visible: PropTypes.bool,
  onRequestClose:
    Platform.OS === 'android' ? PropTypes.func.isRequired : PropTypes.func,
  onShow: PropTypes.func,
  children: PropTypes.node
};

Modal.defaultProps = {
  visible: true
};

export default Modal;
