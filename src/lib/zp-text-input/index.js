import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Platform,
  View,
  TouchableOpacity,
  Animated,
  TextInput
} from 'react-native';
import { TextField } from 'react-native-material-textfield';

import { Colors } from '../themes';
import ZPIcon from '../icomoon';
import Label from './label';

const DEL_ICON_CONTAINER = 40;
const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: Colors.transparent
  },
  input: {
    top: 2,
    padding: 0,
    margin: 0,
    flex: 1
  },
  row: {
    flexDirection: 'row'
  },
  flex: {
    flex: 1
  },
  accessory: {
    top: 2,
    justifyContent: 'center',
    alignSelf: 'flex-start'
  },
  delIconWrapper: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0
  },
  delIcon: {
    width: DEL_ICON_CONTAINER,
    justifyContent: 'center',
    alignItems: 'center'
  },
  labelStyle: {
    paddingLeft: 12
  }
});

class ZPTextInput extends TextField {
  renderDelIcon(value, labelHeight) {
    const delIconWrapper = [
      styles.delIconWrapper,
      {
        top: labelHeight
      }
    ];
    const delIcon = [
      styles.delIcon,
      {
        height: labelHeight
      }
    ];

    const { focused } = this.state;
    if (!value || !focused) {
      return <View />;
    }
    return (
      <View style={delIconWrapper}>
        <TouchableOpacity
          style={delIcon}
          onPress={() => {
            this.input.clear();
            this.onChangeText('');
          }}
        >
          <ZPIcon name="general_del" size={14} />
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const { receivedFocus, focus, focused, errored, text = '' } = this.state;
    const {
      style: inputStyleOverrides,
      label,
      defaultValue,
      characterRestriction: limit,
      editable,
      disabled,
      animationDuration,
      fontSize,
      labelFontSize,
      labelPadding,
      inputContainerPadding,
      labelTextStyle,
      tintColor,
      baseColor,
      textColor,
      errorColor,
      lineWidth,
      activeLineWidth,
      containerStyle,
      inputContainerStyle: inputContainerStyleOverrides,
      errorMessage,
      hideDelIcon,
      hideLabel,
      labelTintColor,
      labelBaseColor,
      labelColor,
      placeholderFontSize,
      ...props
    } = this.props;

    let { height } = this.state;
    let { value, labelHeight } = this.props;

    if (hideLabel) {
      labelHeight = 0;
    }

    if (props.multiline && props.height) {
      /* Disable autogrow if height is passed as prop */
      height = props.height;
    }

    const defaultVisible = !(
      receivedFocus ||
      null != value ||
      null == defaultValue
    );

    value = defaultVisible ? defaultValue : text;

    const active = !!(value || props.placeholder);
    const count = value.length;
    const restricted =
      (errorMessage && errorMessage.length !== 0) || limit <= count;

    const borderBottomColor = restricted
      ? errorColor
      : focus.interpolate({
          inputRange: [-1, 0, 1],
          outputRange: [errorColor, baseColor, tintColor]
        });

    const borderBottomWidth = restricted
      ? activeLineWidth
      : focus.interpolate({
          inputRange: [-1, 0, 1],
          outputRange: [activeLineWidth, lineWidth, activeLineWidth]
        });

    const inputContainerStyle = {
      paddingTop: labelHeight,
      paddingBottom: inputContainerPadding,

      ...(disabled
        ? { overflow: 'hidden' }
        : { borderBottomColor, borderBottomWidth }),

      ...(props.multiline
        ? { height: labelHeight + inputContainerPadding + height }
        : { height: labelHeight + inputContainerPadding + fontSize * 1.5 })
    };

    const inputStyle = {
      fontSize,

      color: disabled || defaultVisible ? baseColor : textColor,

      ...(props.multiline
        ? {
            height: fontSize * 1.5 + height,

            ...Platform.select({
              ios: { top: -1 },
              android: { textAlignVertical: 'top' }
            })
          }
        : { height: fontSize * 1.5 }),
      paddingRight: hideDelIcon ? 0 : DEL_ICON_CONTAINER
    };

    const containerProps = {
      style: containerStyle,
      onStartShouldSetResponder: () => true,
      onResponderRelease: this.onPress,
      pointerEvents: !disabled && editable ? 'auto' : 'none'
    };

    const inputContainerProps = {
      style: [
        styles.inputContainer,
        inputContainerStyle,
        inputContainerStyleOverrides
      ]
    };

    const labelProps = {
      baseSize: labelHeight,
      basePadding: labelPadding,
      fontSize: placeholderFontSize || fontSize,
      activeFontSize: labelFontSize,
      tintColor: labelTintColor || labelColor || tintColor,
      baseColor: labelBaseColor || labelColor || baseColor,
      errorColor,
      animationDuration,
      active,
      focused,
      errored,
      restricted,
      style: labelTextStyle
    };

    return (
      <View {...containerProps}>
        <Animated.View {...inputContainerProps}>
          {hideLabel ? null : (
            <Label {...labelProps}>{errorMessage || label}</Label>
          )}
          <View style={styles.row}>
            {this.renderAffix('prefix', active, focused)}
            <TextInput
              selectionColor={tintColor}
              {...props}
              style={[styles.input, inputStyle, inputStyleOverrides,this.props.style]}              
              editable={!disabled && editable}
              onChange={this.onChange}
              onChangeText={this.onChangeText}
              onContentSizeChange={this.onContentSizeChange}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              value={value}
              ref={this.updateRef}
            />
            {this.renderAffix('suffix', active, focused)}
            {this.renderAccessory()}
          </View>
        </Animated.View>
        {hideDelIcon ? null : this.renderDelIcon(value, labelHeight)}
      </View>
    );
  }
}

ZPTextInput.propTypes = {
  ...TextField.propTypes,
  errorMessage: PropTypes.string,
  hideDelIcon: PropTypes.bool,
  hideLabel: PropTypes.bool
};

ZPTextInput.defaultProps = {
  ...TextField.defaultProps,
  tintColor: Colors.zalo,
  textColor: Colors.defaultText,
  baseColor: Colors.placeholder,
  errorColor: Colors.error,
  fontSize: 16,
  titleFontSize: 12,
  labelFontSize: 12,
  labelHeight: 32,
  labelPadding: 4,
  hideDelIcon: Platform.OS === 'ios',
  labelTextStyle: styles.labelStyle,
  hideLabel: false
};

export default ZPTextInput;
