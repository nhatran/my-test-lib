const console = (function (oldCons) {
    if (!__DEV__) {
      return {
        log: () => { },
        error: () => { },
        warn: () => { },
        info: () => { },
      }
    }
    return { ...oldCons };
  }(window.console));
  
  window.console = console;
  
  export default window.console;
  