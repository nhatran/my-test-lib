import { Dimensions, Platform } from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';

import NumberFormat from './number-format';
import DateFormat from './date-format';
import { Strings, Transaction, IPHONE_X_SIZE } from '../constants';

let timeoutPress;

function zpParserItem(item) {
  console.log('item: ', item);
  try {
    if (!item || item.length === 0) {
      throw new Error();
    }
    const { ext } = JSON.parse(item);
    const result = ext
      .split('\t')
      .map(item => {
        const title = item.split(':', 1)[0];
        const desc = item.replace(`${title}:`, '');
        return { title, desc };
      })
      .filter(item => item.desc);
    console.log('result: ', result);
    return result;
  } catch (e) {
    return [];
  }
}

function isIphoneX() {
  const { height: windowHeight, width: windowWidth } = Dimensions.get('window');
  return (
    Platform.OS === 'ios' &&
    ((windowHeight === IPHONE_X_SIZE.height &&
      windowWidth === IPHONE_X_SIZE.width) ||
      (windowHeight === IPHONE_X_SIZE.width &&
        windowWidth === IPHONE_X_SIZE.height))
  );
}

const statusBarHeight = isIphoneX()
  ? IPHONE_X_SIZE.statusBar
  : Navigator.NavigationBar.Styles.General.StatusBarHeight;
const navBarHeight = Navigator.NavigationBar.Styles.General.NavBarHeight;
const totalNavBarHeight = navBarHeight + statusBarHeight;
const NavigatorBarHeight = {
  status: statusBarHeight,
  bar: navBarHeight,
  total: totalNavBarHeight
};

function singleClick(_function, ...arg) {
  if (timeoutPress) {
    return;
  }
  callFunction(_function, ...arg);
  timeoutPress = setTimeout(() => {
    timeoutPress = undefined;
  }, 500);
}
function callFunction(_function, ...arg) {
  return _function && _function(...arg);
}

function calculateUserCharge(transaction) {
  const { amount, discountamount = 0, userfeeamt = 0, sign } = transaction;
  if (sign === Transaction.Sign.Receiver) {
    return amount - discountamount;
  }
  return amount - discountamount + userfeeamt;
}

function makeBillPayOrderItem(customerName, address) {
  const _customerName = capitalizeEachWord(customerName);
  const _address = capitalizeEachWord(address);
  return `{"ext":"${Strings.TITLE_CUSTOMER_NAME}:${_customerName}\\t${Strings.TITLE_ADDRESS}:${_address}"}`;
}

function capitalizeEachWord(string) {
  return typeof string === 'string'
    ? string.toLowerCase().replace(/(?:^|\s)\S/g, a => a.toUpperCase())
    : string;
}

export default {
  ...NumberFormat,
  ...DateFormat,
  zpParserItem,
  isIphoneX,
  singleClick,
  calculateUserCharge,
  makeBillPayOrderItem,
  capitalizeEachWord,
  callFunction,
  NavigatorBarHeight
};
