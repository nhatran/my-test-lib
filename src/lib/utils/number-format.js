const numberFromString = function(value) {
  if (!value || value.length === 0) {
    return 0;
  }
  const notDot = value.split(/[. ]+/).join('');
  const isNumber = notDot.match(/^[+-]?[0-9]+$/) != null;
  if (!isNumber) {
    return 0;
  }
  return parseInt(notDot);
};

const decimalString = function(num) {
  if (!num || num === 0) {
    return '';
  }
  if (typeof num === 'string') {
    num = numberFromString(num);
  }
  const sign = num < 0 ? '-' : '';
  const result = Math.abs(num)
    .toString()
    .split('')
    .reverse()
    .reduce((acc, num, i) => num + (i && !(i % 3) ? '.' : '') + acc, '');
  return `${sign}${result}`;
};

const formatTransId = function(transid) {
  const transId = String(transid);
  if (!transId || transId.length < 6) {
    return '';
  }

  return `${transId.substring(0, 6)}-${transId.substring(6)}`;
};

const formatCurrency = function(num) {
  return decimalString(num) + ' VND';
};

export default {
  decimalString,
  numberFromString,
  formatTransId,
  formatCurrency
};
