import moment from 'moment';

const stringFromTimeStamp = function(timeStamp, formatters) {
  if (timeStamp) {
    const time = moment(timeStamp, 'x').utcOffset('+07:00');
    return time.format(formatters);
  }
  return '';
};

const dateString = function(timeStamp) {
  return stringFromTimeStamp(timeStamp, 'DD/MM/YYYY HH:mm');
};

const hourAndDayString = function(timeStamp) {
  return stringFromTimeStamp(timeStamp, 'DD/MM HH:mm');
};

const dayString = function(timeStamp) {
  return stringFromTimeStamp(timeStamp, 'DD/MM');
};

const monthAndYearString = function(timeStamp) {
  return stringFromTimeStamp(timeStamp, 'MM/YYYY');
};

const formatDate = function(timeStr, formatFrom, formatTo) {
  return moment(timeStr, formatFrom).format(formatTo);
};

export default {
  dateString,
  hourAndDayString,
  dayString,
  monthAndYearString,
  stringFromTimeStamp,
  formatDate
};
