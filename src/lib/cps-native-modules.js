import { NativeModules } from 'react-native';
import { ZPDialogType, Strings } from './constants';

const ZaloPayIAP = NativeModules.ZaloPay;

function getNativeModules(){
  return ZaloPayIAP;
}
function showLoading() {
  ZaloPayIAP && ZaloPayIAP.showLoading();
}

function hideLoading() {
  ZaloPayIAP && ZaloPayIAP.hideLoading();
}

function request(url, requestData) {
  if (!ZaloPayIAP) {
    return Promise.reject();
  }
  return ZaloPayIAP.request && ZaloPayIAP.request(url, requestData);
}

function showDialog(type, title, message, buttons) {
  return ZaloPayIAP.showDialog && ZaloPayIAP.showDialog(type, title, message, buttons);
}

function showNoConnectionDialog(){
  return ZaloPayIAP.showDialog && ZaloPayIAP.showDialog(ZPDialogType.NoConnection, '','',[Strings.BUTTON_CLOSE]);
}

function showDefaultErrorDialog(){
  return ZaloPayIAP.showDialog && ZaloPayIAP.showDialog(ZPDialogType.Info, Strings.DIALOG_TITLE, Strings.DEFAULT_ERROR_MESSAGE, [Strings.BUTTON_CLOSE]);
}
function getUserInfo() {
  return ZaloPayIAP.getUserInfo && ZaloPayIAP.getUserInfo();
}

function logout() {
  ZaloPayIAP.logout && ZaloPayIAP.logout();
}

function payOrder(payData) {
  return ZaloPayIAP.payOrder && ZaloPayIAP.payOrder(payData);
}

function closeModule(moduleId) {
  ZaloPayIAP.closeModule && ZaloPayIAP.closeModule(moduleId);
}


export default {
  showLoading,
  hideLoading,
  request,
  showDialog,
  showNoConnectionDialog,
  showDefaultErrorDialog,
  getUserInfo,
  logout,
  payOrder,
  closeModule,
  getNativeModules,
};
