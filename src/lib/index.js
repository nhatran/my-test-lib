//Generate
import Utils from './utils';
import Fonts from './themes/fonts';
import Colors from './themes/colors';
import ZPIcon from './icomoon';
import Strings from './constants/strings';
import Events from './constants/events';
import ZPErrorCode from './constants/zp-error-code';
import Transaction from './constants/transaction';
import { ZPButtonType, ZPDialogType, IPHONE_X_SIZE } from './constants';
import { setZaloPayApi, getZaloPayApi } from './generate';

import createIcon from './icomoon/create-icon';
import ZPFontManager from './icomoon/font-manager';
import ZPPlaceHolderView from './zp-place-holder';
import ZPTextInput from './zp-text-input';
import ZPModal from './zpmodal';
import NetStatus from './net-status';

import {
  ZPDetailComponent,
  CPSDetailComponent,
  Bill,
  BillOrder
} from './detail-component';

import NavBar from './navigator/exnavbar';
import ZPNavigator from './navigator';

import console from './console';
import CPSNativeModules from './cps-native-modules';

import { ZPListView, ZPSwipeListView, MoreButton } from './extension-list-view';
import ZPDialog from './common/zpdialog';

import {
  ZPButton,
  DefaultButton,
  DetailButton,
  CloseButton,
  RetryButton,
  DismissKeyboardView,
  Line,
  ZPComponent,
  MoneyView,
  AvatarImage,
  ZPImage,
  ToastView,
  AmountView,
  KeyboardSpacer
} from './common';

export {
  Utils,
  Fonts,
  Colors,
  ZPIcon,
  Strings,
  Events,
  ZPErrorCode,
  Transaction,
  ZPButtonType,
  ZPDialogType,
  IPHONE_X_SIZE
};
export { setZaloPayApi, getZaloPayApi };
export { NavBar, ZPNavigator };

export { ZPListView, ZPSwipeListView, MoreButton };

export {
  ZPModal,
  ZPTextInput,
  ZPPlaceHolderView,
  NetStatus,
  ZPDetailComponent,
  ZPFontManager,
  createIcon,
  CPSDetailComponent,
  Bill,
  BillOrder,
  console,
  CPSNativeModules
};

export { ZPButton, DefaultButton, DetailButton, CloseButton, RetryButton };

export {
  DismissKeyboardView,
  Line,
  ZPComponent,
  ZPDialog,
  MoneyView,
  AvatarImage,
  ZPImage,
  ToastView,
  AmountView,
  KeyboardSpacer
};
