import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions, Image } from 'react-native';
import { ZPButtonType, ZPIcon } from '../generate';
import { ZPButton } from '../common';
import { Colors, Fonts } from '../themes';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.defaultBackground
  },
  view: {
    alignItems: 'center'
  },
  text: {
    fontSize: Fonts.textSize,
    fontFamily: Fonts.regular,
    color: Colors.placeholder,
    margin: 20,
    marginTop: 16,
    textAlign: 'center',
    lineHeight: 20
  },
  icon: {
    marginTop: 70
  },
  button: {
    marginTop: 20
  }
});

class ZPPlaceHolderView extends React.Component {
  _sources = function(image) {
    switch (image) {
      case 'comingSoon':
        return require('./images/comingsoon.png');
      case 'nocontact':
        return require('./images/nocontact.png');
      case 'nodeal':
        return require('./images/nodeal.png');
      case 'noload':
        return require('./images/noload.png');
      case 'nonotify':
        return require('./images/nonotify.png');
      case 'processing':
        return require('./images/processing.png');
      case 'unsupport':
        return require('./images/unsupport.png');
      case 'maintenance':
        return require('./images/maintenance.png');
      case 'disconnect':
        return require('./images/disconnect.png');
      case 'recentbill' :
        return require('./images/recentbill.png');
      default:
        return require('./images/noload.png');
    }
  };

  renderImage() {
    const { imageName, iconName, imageSource, imageStyle } = this.props;
    const _iconStyle = [styles.icon, imageStyle];
    if (imageSource) {
      return <Image style={_iconStyle} source={imageSource} />;
    } else if (imageName) {
      return <Image style={_iconStyle} source={this._sources(imageName)} />;
    } else if (iconName) {
      return <ZPIcon style={_iconStyle} name={iconName} size={width / 3} />;
    }
    return <View />;
  }

  renderDescription() {
    const { description, descriptionStyle } = this.props;
    return <Text style={[styles.text, descriptionStyle]}>{description}</Text>;
  }

  renderButton() {
    const { buttonTitle, onPress, buttonType } = this.props;
    return <ZPButton buttonType={buttonType} style={styles.button} text={buttonTitle} onPress={onPress} />;
  }

  render() {
    const { showButton, style, children, buttonType } = this.props;
    const viewStyle = buttonType === ZPButtonType.Retry ? { flex: 1 } : {};
    return (
      <View style={[styles.container, style]}>
        <View style={[styles.view, viewStyle]}>
          {this.renderImage()}
          {this.renderDescription()}
          {children}
          {showButton && buttonType === ZPButtonType.Retry ? this.renderButton() : null}
        </View>
        {showButton && buttonType !== ZPButtonType.Retry ? this.renderButton() : null}
      </View>
    );
  }
}

ZPPlaceHolderView.propTypes = {
  ...View.propTypes,
  style: PropTypes.oneOfType([PropTypes.number, PropTypes.array, PropTypes.object]) ,
  imageName: PropTypes.string,
  iconName: PropTypes.string,
  description: PropTypes.string,
  children: PropTypes.node,
  onPress: PropTypes.func,
  showButton: PropTypes.bool,
  buttonTitle: PropTypes.string,
  buttonType: PropTypes.number
};
ZPPlaceHolderView.defaultProps = {
  buttonType: ZPButtonType.Default,
  showButton: false
};

export default ZPPlaceHolderView;
