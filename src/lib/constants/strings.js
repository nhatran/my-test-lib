const BUTTON_CLOSE = 'Đóng';
const BUTTON_OK = 'Đồng ý';
const BUTTON_REGISTER = 'Đăng Ký';
const BUTTON_PAY = 'Thanh Toán';

const DIALOG_TITLE = 'Thông báo';
const DIALOG_ERROR_TITLE = 'Lỗi';
const SEE_MORE = 'XEM THÊM';

const DEFAULT_ERROR_MESSAGE = 'Có lỗi xảy ra, vui lòng thử lại sau.';

const TITLE_ZPTRANSID = 'Mã giao dịch';
const TITLE_APP_TRANSID = 'Mã đơn hàng';
const TITLE_TIME = 'Thời gian';
const TITLE_TRANS_TYPE = 'Loại dịch vụ';
const TITLE_FEE = 'Phí giao dịch';
const TITLE_SERVICE = 'Dịch vụ';
const TITLE_GIFT = 'Phiếu quà tặng';
const TITLE_INITIAL_AMOUNT = 'Giá ban đầu';
const TITLE_TELCO_PROVIDER = 'Nhà mạng';
const TITLE_PROVIDER = 'Nhà cung cấp';
const TITLE_QUANTITY = 'Số lượng';
const TITLE_STATUS = 'Trạng thái';
const TITLE_USER_CODE = 'Mã KH';
const TITLE_CUSTOMER_NAME = 'Tên KH';
const TITLE_ADDRESS = 'Địa chỉ';

const DESC_FEE_FREE = 'Miễn phí';
const COPY_TEXT = 'Sao chép';

const MOBILE_TOPUP_SERVICE = 'Nạp tiền điện thoại';
const ELECTRIC_SERVICE = 'Thanh toán tiền điện';
const INTERNET_SERVICE = 'Cước Internet';
const TV_SERVICE = 'Cước truyền hình';
const WATER_SERVICE = 'Thanh toán tiền nước';
const MOBILE_CARD_SERVICE = 'Thẻ điện thoại';

const ELECTRIC_MESSAGE = 'Tiền điện';
const INTERNET_MESSAGE = 'Cước Internet';
const TV_MESSAGE = 'Cước truyền hình';
const WATER_MESSAGE = 'Tiền nước';

const ServiceType = {
  MOBILE_TOPUP: 1,
  ELECTRIC: 2,
  INTERNET: 3,
  TV: 4,
  WATER: 5,
  MOBILE_CARD: 6
};

const ServicePaymentMessage = {
  [ServiceType.MOBILE_TOPUP]: MOBILE_TOPUP_SERVICE,
  [ServiceType.ELECTRIC]: ELECTRIC_SERVICE,
  [ServiceType.INTERNET]: INTERNET_SERVICE,
  [ServiceType.TV]: TV_SERVICE,
  [ServiceType.WATER]: WATER_SERVICE,
  [ServiceType.MOBILE_CARD]: MOBILE_CARD_SERVICE
};

const ServiceMessage = {
  [ServiceType.ELECTRIC]: ELECTRIC_MESSAGE,
  [ServiceType.INTERNET]: INTERNET_MESSAGE,
  [ServiceType.TV]: TV_MESSAGE,
  [ServiceType.WATER]: WATER_MESSAGE
};

export default {
  BUTTON_CLOSE,
  BUTTON_OK,
  BUTTON_REGISTER,
  BUTTON_PAY,
  DIALOG_TITLE,
  DIALOG_ERROR_TITLE,
  DEFAULT_ERROR_MESSAGE,
  SEE_MORE,
  TITLE_ZPTRANSID,
  TITLE_APP_TRANSID,
  TITLE_TIME,
  TITLE_TRANS_TYPE,
  TITLE_FEE,
  TITLE_SERVICE,
  TITLE_GIFT,
  TITLE_INITIAL_AMOUNT,
  TITLE_TELCO_PROVIDER,
  TITLE_PROVIDER,
  TITLE_QUANTITY,
  TITLE_STATUS,
  DESC_FEE_FREE,
  TITLE_USER_CODE,
  TITLE_CUSTOMER_NAME,
  TITLE_ADDRESS,
  ServiceType,
  ServicePaymentMessage,
  ServiceMessage,
  COPY_TEXT,
};
