const ZPButtonType = {
  Default: 1,
  Detail: 2,
  Close: 3,
  Retry: 4
};

const ZPDialogType = {
  General: 0,
  Error: 1,
  Success: 2,
  Warning: 3,
  Info: 4,
  NoConnection: 5
};

const IPHONE_X_SIZE = {
  width: 375,
  height: 812,
  statusBar: 44,
  bottomPadding: 34
};

export { ZPButtonType, ZPDialogType, IPHONE_X_SIZE };
