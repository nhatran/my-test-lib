import React from 'react';
import PropTypes from 'prop-types';
import { View, ListView, StyleSheet, Text } from 'react-native';
import Dash from 'react-native-dash';

import { Colors, Fonts, Strings} from '../generate';
import DetailRow from './detail-row';
import { AmountView, ToastView } from '../common/';

const styles = StyleSheet.create({
  container: {
    flex:1,
  },
  listContainer: {
    flex: 1,
    backgroundColor: Colors.white
  },
  headerContainer: {
    flex: 1,
    padding: 10,
  },
  dash: {
    flex: 1,
    marginTop: 10
  },
  dashColor: {
    backgroundColor: Colors.line
  },
  headerDesc: {
    fontStyle: 'italic',
    fontFamily: Fonts.regular,
    fontSize: Fonts.textSize,
    textAlign: 'center',
    color: Colors.subText
  }
});

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2
    });
    this.renderRow = this.renderRow.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.handleShowToast = this._showToast.bind(this);
    this.state = {
      showToast: false,
    };
  }

  _showToast(isShow) {
    this.setState({ showToast: isShow });
  }

  renderRow(data){
    return <DetailRow {...data} onShowToast={this.handleShowToast} />;
  }
  
  renderHeader() {
    return (
      <View style={styles.headerContainer}>
        <AmountView amount={this.props.amount} />
        <Text style={styles.headerDesc}>{this.props.description}</Text>
        <Dash
          style={styles.dash}
          dashStyle={styles.dashColor}
          dashGap={5}
          dashLength={6}
          dashThickness={1}
        />
      </View>
    );
  }
  render() {
    const { data } = this.props;
    return (
      <View style={styles.container}>
        <ListView
          style={styles.listContainer}
          dataSource={this.dataSource.cloneWithRows(data)}
          renderRow={this.renderRow}
          renderHeader={this.renderHeader}
          removeClippedSubviews={false}
          {...this.props}
        />
        <ToastView text={Strings.COPY_TEXT} showToast={this.state.showToast} onShowToast={this.handleShowToast} />
      </View>
    );
  }
}
Detail.propTypes = {
  amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  description: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.shape({}))
};

export default Detail;
