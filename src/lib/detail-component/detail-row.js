import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text, Clipboard, TouchableWithoutFeedback } from 'react-native';
import { Utils, Colors, Fonts } from '../generate';

const styles = StyleSheet.create({
  containerWrapper: {
    flex: 1
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingTop: 7,
    paddingBottom: 7
  },
  title: {
    fontFamily: Fonts.regular,
    fontSize: Fonts.textSize,
    color: Colors.subText,
    marginRight: 30
  },
  desc: {
    fontFamily: Fonts.regular,
    fontSize: Fonts.textSize,
    textAlign: 'right',
    color: Colors.subText
  },
  vnd: {
    fontFamily: Fonts.regular,
    fontSize: Fonts.subTextSize,
    textAlign: 'right',
    color: Colors.subText
  },
  flexFull: {
    flex: 1
  }
});
const AmountView = props => (
  <Text style={styles.desc}>
    {Utils.decimalString(props.amount)}
    <Text style={styles.vnd}>{' VND'}</Text>
  </Text>
);
AmountView.propTypes = {
  amount: PropTypes.number
};

async function copyText(props) {
  console.log('copy text', props.decs);
  Clipboard.setString(props.desc);
  try {
    await Clipboard.getString();
    props.onShowToast && props.onShowToast(true);
  } catch (e) {
    console.log(e);
  }
}

const Row = props => {
  const { desc, subDesc, amount, title, multiLineDesc, canCopy } = props;
  const numberOfLines = multiLineDesc ? 3 : 1;

  const childrens = [];
  if (desc) {
    const descStyle = [styles.desc, { color: props.color ? props.color : Colors.subText }];

    if (subDesc && subDesc.length > 0) {
      childrens.push(
        <Text style={[styles.desc, styles.flexFull]} numberOfLines={1} key={'subDesc'}>
          {subDesc}
        </Text>
      );
    } else {
      descStyle.push(styles.flexFull);
    }
    childrens.push(
      <Text style={descStyle} numberOfLines={numberOfLines} key={'desc'}>
        {desc}
      </Text>
    );
  } else if (amount) {
    childrens.push(<AmountView amount={amount} key={'amount'} />);
  }

  if (canCopy) {
    return (
      <TouchableWithoutFeedback style={styles.containerWrapper} onPress={() => copyText(props)}>
        <View style={styles.container}>
          <Text style={styles.title}>{title}</Text>
          {childrens}
        </View>
      </TouchableWithoutFeedback>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {childrens}
    </View>
  );
};

Row.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  subDesc: PropTypes.string,
  color: PropTypes.string,
  amount: PropTypes.number,
  multiLineDesc: PropTypes.bool,
  canCopy: PropTypes.bool
};

export default Row;
