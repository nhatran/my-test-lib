import ZPDetailComponent from './zp-detail';
import { CPSDetailComponent, Bill, BillOrder } from './cps-detail';

export { ZPDetailComponent, CPSDetailComponent, Bill, BillOrder };
