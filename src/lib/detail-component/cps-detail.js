import React from 'react';
import PropTypes from 'prop-types';
import { findIndex } from 'lodash';

import ZPDetailComponent from './zp-detail';
import DetailView from './detail-view';
import { Strings } from '../constants';
import Utils from '../utils';

class CPSDetailComponent extends ZPDetailComponent {
  parseData() {
    const datas = super.parseData();
    const { detailType } = this.props;
    const service = {
      title: Strings.TITLE_SERVICE,
      desc: Strings.ServicePaymentMessage[detailType]
    };
    datas.splice(0, 0, service);
    return datas;
  }
}

CPSDetailComponent.propTypes = {
  detailType: PropTypes.number
};

class Bill extends CPSDetailComponent {
  constructor(props) {
    super(props);
    this.description = this.description.bind(this);
  }

  description() {
    const { item, detailType } = this.props;
    const messages = [];
    item &&
      item.forEach(element => {
        if (element.m) {
          messages.push(element.m);
        } else if (element.month) {
          messages.push(element.month);
        }
      });
    return `${Strings.ServiceMessage[detailType]} ${messages.join(' ,')}`;
  }

  getStatusStr = function(status) {
    switch (status) {
      case 100:
        return 'Thất bại';
      case 80:
        return 'Đã hoàn tiền';
      default:
        return 'Thành công';
    }
  };

  parseData() {
    const datas = super.parseData();

    const { supplierCode, customerCode, status, transactionDate, extItem, zptransid, apptransid } = this.props;
    let extendItems = Utils.zpParserItem(extItem);
    extendItems = extendItems.map(item => {
      item.multiLineDesc = true;
      item.desc = Utils.capitalizeEachWord(item.desc);
      return item;
    });

    const statusStr = this.getStatusStr(status);
    const timeStamp = Utils.formatDate(transactionDate, 'YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm');
    datas.splice(1, 0, ...[{ title: Strings.TITLE_PROVIDER, desc: supplierCode }]);
    datas.push({ title: Strings.TITLE_USER_CODE, desc: customerCode, canCopy: true },...extendItems);
    
    const information = [{ title: Strings.TITLE_STATUS, desc: statusStr }, { title: Strings.TITLE_TIME, desc: timeStamp }];
    let order = datas.length;
    if (apptransid) {
      order = findIndex(datas, { desc: apptransid });
    } else if (zptransid) {
      order = findIndex(datas, { desc: zptransid });
    }
    datas.splice(order, 0, ...information);

    return datas;
  }

  render() {
    const userCharge = Utils.calculateUserCharge(this.props);
    const description = this.description();
    return <DetailView {...this.props} description={description} amount={userCharge} data={this.parseData()} />;
  }
}

class BillOrder extends Bill {
  
  parseData() {
    const { supplierCode, customerCode, customerName, customerAddress } = this.props;
    const datas = [{ title: Strings.TITLE_PROVIDER, desc: supplierCode },{ title: Strings.TITLE_USER_CODE, desc: customerCode}];

    customerName && datas.push({
        title: Strings.TITLE_CUSTOMER_NAME,
        desc: Utils.capitalizeEachWord(customerName),
        multiLineDesc: true
      });
    customerAddress && datas.push({
        title: Strings.TITLE_ADDRESS,
        desc: Utils.capitalizeEachWord(customerAddress),
        multiLineDesc: true
      });
    
    return datas;
  }

  render() {
    const description = this.description();
    return <DetailView {...this.props} description={description}  data={this.parseData()} />;
  }
}

export { CPSDetailComponent, Bill, BillOrder };
