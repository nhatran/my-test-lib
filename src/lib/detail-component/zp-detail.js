import React from 'react';
import PropTypes from 'prop-types';
import DetailView from './detail-view';
import Utils from '../utils';

import { Strings } from '..';

class ZPDetailComponent extends React.Component {
  //eslint-disable-next-line
  parseItem(item) {
    return Utils.zpParserItem(item);
  }
  
  parseData() {
    let result = [];
    const { zptransid, timestamp, item, transType, userfeeamt, apptransid, amount, discountamount } = this.props;
    if (transType && transType.length > 0) {
      result.push({
        title: Strings.TITLE_TRANS_TYPE,
        desc: transType
      });
    }

    const itemArray = this.parseItem(item);
    if (itemArray && itemArray.length > 0) {
      result = result.concat(itemArray);
    }

    if (discountamount && parseInt(discountamount) > 0) {
      result.push({
        title: Strings.TITLE_INITIAL_AMOUNT,
        amount
      });
      result.push({
        title: Strings.TITLE_GIFT,
        amount: -discountamount
      });
    }
    if (userfeeamt && userfeeamt > 0) {
      result.push({
        title: Strings.TITLE_FEE,
        amount: userfeeamt
      });
    } else {
      result.push({
        title: Strings.TITLE_FEE,
        desc: Strings.DESC_FEE_FREE
      });
    }

    if (timestamp && timestamp > 0) {
      result.push({
        title: Strings.TITLE_TIME,
        desc: Utils.dateString(timestamp)
      });
    }
    if (apptransid) {
      result.push({
        title: Strings.TITLE_APP_TRANSID,
        desc: apptransid
      });
    }
    if (zptransid) {
      result.push({
        title: Strings.TITLE_ZPTRANSID,
        desc: Utils.formatTransId(zptransid)
      });
    }
    return result;
  }

  render() {
    const userCharge = Utils.calculateUserCharge(this.props);
    return <DetailView {...this.props} amount={userCharge} data={this.parseData()} />;
  }
}

ZPDetailComponent.propTypes = {
  zptransid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  apptransid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  timestamp: PropTypes.number,
  item: PropTypes.string,
  transType: PropTypes.string,
  userfeeamt: PropTypes.number,
  amount: PropTypes.number,
  discountamount: PropTypes.number,
};

export default ZPDetailComponent;
