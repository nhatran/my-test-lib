import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import ZPIcon from '../icomoon';

const styles = StyleSheet.create({
  background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    flex: 1
  }
});

class ZPImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false
    };
    this.handleImageLoad = this.handleImageLoad.bind(this);
  }
  handleImageLoad() {
    this.setState({
      isLoaded: true
    });
  }

  style() {
    return this.props.style;
  }

  renderBackground(style) {
    const { isLoaded } = this.state;
    const { defaultSource, defaultIcon, source, uri } = this.props;
    if (!isLoaded || !(source || uri)) {
      if (defaultSource) {
        return (
          <View style={[styles.background, style]}>
            <Image source={defaultSource} />
          </View>
        );
      } else if (defaultIcon) {
        const { color, iconSize } = this.props;
        return (
          <View style={[styles.background, style]}>
            <ZPIcon name={defaultIcon} color={color} size={iconSize} />
          </View>
        );
      }
    }
  }

  render() {
    const style = this.style();
    const { uri, source } = this.props;
    const _source = uri ? { uri } : source;
    return (
      <View style={[style, this.props.style]}>
        {this.renderBackground(style)}
        <Image {...this.props} source={_source} style={[styles.image, style]} onLoad={this.handleImageLoad} />
      </View>
    );
  }
}
ZPImage.propTypes = {
  ...Image.propTypes
};
export default ZPImage;
