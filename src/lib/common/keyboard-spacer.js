import React from 'react';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { IPHONE_X_SIZE } from '../constants';
import Utils from '../utils';

export default class CustomKeyboardSpacer extends KeyboardSpacer {
  render() {
    return (
      <KeyboardSpacer
        topSpacing={Utils.isIphoneX() ? -IPHONE_X_SIZE.bottomPadding : 0}
      >
        {this.props.children}
      </KeyboardSpacer>
    );
  }
}
