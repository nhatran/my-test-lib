import React from 'react';
import { Platform, Keyboard, NetInfo } from 'react-native';

export default class ZPComponent extends React.Component {
  constructor(props) {
    super(props);

    this.listeners = [];
    this.isKeyboardOpened = false;
    this.isConnected = false;

    this.showKeyboard = this.showKeyboard.bind(this);
    this.hideKeyboard = this.hideKeyboard.bind(this);
    this.handleChangeNetwork = this.handleChangeNetwork.bind(this);
  }

  componentWillUnmount() {
    this._removeEventListeners();
    this._removeNetworkEvent();
  }

  _removeEventListeners() {
    if (this.listeners) {
      this.listeners.forEach(listener => listener.remove());
    }
  }

  _removeNetworkEvent() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleChangeNetwork
    );
  }

  registerComponentFocus() {
    const currentRoute = this.props.navigator.navigationContext.currentRoute;
    const { navigationContext } = this.props.navigator;
    this.listeners = [
      navigationContext.addListener('willfocus', event => {
        if (currentRoute === event.data.route) {
          console.log('componentWillFocus');
          this.componentWillFocus && this.componentWillFocus();
        } else {
          console.log('componentWillDefocus');
          this.componentWillDefocus && this.componentWillDefocus();
        }
      }),
      navigationContext.addListener('didfocus', event => {
        if (currentRoute === event.data.route) {
          console.log('componentDidFocus');
          this.componentDidFocus && this.componentDidFocus();
        } else {
          console.log('viewDidDisappear');
          this.componentDidDefocus && this.componentDidDefocus();
        }
      })
    ];
  }

  registerNetworkEvent() {
    const networkChangeListener = 'connectionChange';
    NetInfo.isConnected
      .fetch()
      .then()
      .done(isConnected => {
        if (Platform.OS === 'android') {
          this.handleChangeNetwork(isConnected);
        }
        NetInfo.isConnected.addEventListener(
          networkChangeListener,
          this.handleChangeNetwork
        );
      });
  }

  registerKeyboardEvent() {
    const showListener = Platform.select({
      android: 'keyboardDidShow',
      ios: 'keyboardWillShow'
    });
    const hideListener = Platform.select({
      android: 'keyboardDidHide',
      ios: 'keyboardWillHide'
    });
    this.listeners = [
      Keyboard.addListener(showListener, this.showKeyboard),
      Keyboard.addListener(hideListener, this.hideKeyboard)
    ];
  }

  showKeyboard() {
    this.isKeyboardOpened = true;
  }

  hideKeyboard() {
    this.isKeyboardOpened = false;
  }

  handleChangeNetwork(isConnected) {
    console.log(`Network connect, is ${isConnected ? 'online' : 'offline'}`);
    if (this.isConnected !== isConnected) {
      this.isConnected = isConnected;
    }
  }
}

ZPComponent.propTypes = {
  ...React.Component.propTypes
};
