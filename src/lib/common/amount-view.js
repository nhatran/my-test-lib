import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';
import { Utils, Fonts, Colors } from '../generate';

const CURRENCY_WIDTH = 40;

const styles = StyleSheet.create({
    amount_container: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 40,
        marginBottom: 40
    },
    amount_sub: {
        width: CURRENCY_WIDTH,
        fontFamily: Fonts.regular,
        fontSize: Fonts.textSize,
        color: Colors.subText
    },
    amount_main: {
        fontFamily: Fonts.medium,
        color: Colors.defaultText
    },
});

const AmountView = props => {
    let { amount } = props;
    if (typeof amount === 'string') {
        amount = Utils.numberFromString(amount);
    }
    let fontSize;
    if (amount < 100000) {
        fontSize = 48;
    } else if (amount >= 1000000) {
        fontSize = 38;
    } else {
        fontSize = 42;
    }
    const style = {
        fontSize
    };
    return (
      <View style={styles.amount_container}>
        <View style={{ width: CURRENCY_WIDTH }} />
        <Text style={[styles.amount_main, style]}>
          {Utils.decimalString(amount)}
          <Text style={styles.amount_sub}>{' VND'}</Text>
        </Text>
      </View>
    );
};

AmountView.propTypes = {
    amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default  AmountView ;

export { AmountView };
