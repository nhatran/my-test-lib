import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import ZPImage from './zpimage';

class AvatarImage extends ZPImage {
  style() {
    const { borderRadius } = this.props;
    let { size } = this.props;
    if (size === undefined && borderRadius) {
      size = borderRadius * 2;
    }
    return {
      width: size,
      height: size,
      borderRadius: size / 2
    };
  }
}

AvatarImage.propTypes = {
  style: Image.propTypes.style,
  borderRadius: PropTypes.number,
  size: PropTypes.number.isRequired,
  iconColor: PropTypes.string,
  uri: PropTypes.string
};
AvatarImage.defaultProps = {
  defaultIcon: 'profile_avatadefault',
  iconSize: 40
};

export default AvatarImage;
