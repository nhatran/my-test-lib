import { Strings } from '../constants';
import {
  getZaloPayApi
} from '../generate';

const { BUTTON_CLOSE, DIALOG_TITLE } = Strings;

const NETWORK_CONNECTION_LOST = -1005;
const NOT_CONNECTED_TO_INTERNET = -1009;

const DialogTypes = {
  None: 0,
  Error: 1,
  Success: 2,
  Warning: 3,
  Notification: 4,
  NoInternet: 5
};

const isNoInternetConnection = code => {
  if (code === NETWORK_CONNECTION_LOST || code === NOT_CONNECTED_TO_INTERNET) {
    return true;
  }
  return false;
};

const showDialog = function(type, title, message, buttons, handle) {
  const ZaloPayApi = getZaloPayApi();
  ZaloPayApi && ZaloPayApi.showDialog(type, title, message, buttons).then(response => {
    if (handle) {
      handle(response.code);
    }
  });
};

const warningNoInternetConnection = handle => {
  showDialog(
    DialogTypes.NoInternet,
    DIALOG_TITLE,
    'Kiểm tra kết nối mạng',
    [BUTTON_CLOSE],
    handle
  );
};

const showDialogError = function(code, message, handle) {
  if (isNoInternetConnection(code)) {
    warningNoInternetConnection(handle);
    return;
  }
  if (message && message.length > 0) {
    showDialog(
      DialogTypes.Notification,
      DIALOG_TITLE,
      message,
      [BUTTON_CLOSE],
      handle
    );
  }
};

const showDialogNotification = function(message, buttons, handle) {
  if (buttons === undefined) {
    buttons = [BUTTON_CLOSE];
  }
  showDialog(DialogTypes.Notification, DIALOG_TITLE, message, buttons, handle);
};

export default {
  showDialogNotification,
  showDialogError,
  showDialog,
  warningNoInternetConnection,
  isNoInternetConnection,
  DialogTypes
};

export { DialogTypes };
