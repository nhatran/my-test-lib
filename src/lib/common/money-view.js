import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';

import Utils from '../utils';
import { Colors, Fonts } from '../themes';

const LARGE_FONT = 20;
const SMALL_FONT = 13;
const CURRENCY_WIDTH = 40;

/* eslint-disable react-native/no-unused-styles */
const styles = StyleSheet.create({
  text: {
    fontFamily: Fonts.regular,
    backgroundColor: Colors.transparent,
    fontSize: LARGE_FONT
  },
  /* eslint-enable react-native/no-unused-styles */
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  }
});

const convertToString = value => {
  if (typeof value === 'number') {
    value = Utils.decimalString(value);
  }
  if (value === undefined || value.length === 0) {
    value = '0';
  }
  return value;
};

const signSymbol = (hasSign, sign) => {
  if (!hasSign) {
    return '';
  }
  if (sign) {
    return '+ ';
  }
  return '- ';
};

const colorStyle = (hasSign, negativeSign) => (hasSign && negativeSign ? { color: Colors.pay } : { color: Colors.defaultText });
const MoneyView = props => {
  const { value, hasSign, negativeSign, style, isSuffixFormat, hasCurrencySymbol, currencySymbolStyle, centerAmount } = props;
  const valueText = convertToString(value);
  const sign = signSymbol(hasSign, negativeSign);
  const prefix = valueText.substring(0, valueText.length - 4);
  const suffix = valueText.substring(valueText.length - 4, valueText.length);
  const largeStyle = [styles.text, colorStyle(hasSign, negativeSign), style];
  const smallStyle = [largeStyle, { fontSize: SMALL_FONT }];
  if (!isSuffixFormat) {
    return (
      <View style={styles.rowContainer}>
        {hasCurrencySymbol && centerAmount ? <View style={{ width: CURRENCY_WIDTH }} /> : null}
        <Text style={largeStyle}>
          {sign}
          {valueText}
          {hasCurrencySymbol ? <Text style={[smallStyle, currencySymbolStyle, { width: CURRENCY_WIDTH }]}>{' VND'}</Text> : null}
        </Text>
      </View>
    );
  }
  return (
    <View style={styles.rowContainer}>
      {hasCurrencySymbol && centerAmount ? <View style={{ width: CURRENCY_WIDTH }} /> : null}
      <Text style={largeStyle}>
        {sign}
        {prefix}
        <Text style={smallStyle}>{suffix}</Text>
        {hasCurrencySymbol ? <Text style={[smallStyle, currencySymbolStyle, { width: CURRENCY_WIDTH }]}>{' VND'}</Text> : null}
      </Text>
    </View>
  );
};
MoneyView.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  hasSign: PropTypes.bool,
  negativeSign: PropTypes.bool,
  style: PropTypes.number,
  isSuffixFormat: PropTypes.bool,
  currencySymbolStyle: Text.propTypes.style,
  hasCurrencySymbol: PropTypes.bool,
  centerAmount: PropTypes.bool
};
MoneyView.defaultProps = {
  value: 0,
  hasSign: false,
  isSuffixFormat: false,
  hasCurrencySymbol: false,
  centerAmount: false
};

export default MoneyView;
