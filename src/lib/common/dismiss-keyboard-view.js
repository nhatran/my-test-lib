import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableWithoutFeedback } from 'react-native';

import DismissKeyboard from 'react-native-dismiss-keyboard';

const DismissKeyboardView = props => (
  <TouchableWithoutFeedback
    onPress={() => {
      DismissKeyboard();
    }}
  >
    <View {...props}>{props.children}</View>
  </TouchableWithoutFeedback>
);
DismissKeyboardView.propTypes = {
  children: PropTypes.node
};

export default DismissKeyboardView;
