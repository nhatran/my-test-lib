import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import Dash from 'react-native-dash';

import { Colors } from '../themes';

const styles = StyleSheet.create({
  container: {
    height: 1
  },
  color: {
    backgroundColor: Colors.line
  }
});

class Line extends React.Component {
  render() {
    const style = [];
    if (this.props.style) {
      style.push(this.props.style);
    }
    if (this.props.marginLeft) {
      style.push({ marginLeft: this.props.marginLeft });
    }
    if (this.props.type === 'dash') {
      return <Dash style={[styles.container, style]} dashStyle={styles.color} dashGap={5} dashLength={6} dashThickness={1} />;
    }
    if (this.props.backgroundColor) {
      return (
        <View style={{ backgroundColor: this.props.backgroundColor }}>
          <View style={[styles.container, styles.color, style]} />
        </View>
      );
    }
    return <View style={[styles.container, styles.color, style]} />;
  }
}
Line.propTypes = {
  backgroundColor: PropTypes.string,
  marginLeft: PropTypes.number,
  style: PropTypes.number,
  type: PropTypes.string
};
export default Line;
