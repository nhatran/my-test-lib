import React from 'react';
import { View, StyleSheet, TouchableHighlight, Text } from 'react-native';
import PropTypes from 'prop-types';

import { Colors, Fonts, ZPButtonType } from '../generate';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  buttonStyle: {
    alignItems: 'center',
    padding: 16,
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 3
  },
  buttonText: {
    fontSize: Fonts.buttonFontSize,
    color: Colors.white,
    fontFamily: Fonts.regular
  },
  /*eslint-disable react-native/no-unused-styles */
  defaultBg: {
    backgroundColor: Colors.zalo,
    padding: 16,
    borderRadius: 3,
    alignItems: 'center'
  },
  detailBg: {
    backgroundColor: Colors.subButton
  },
  closeBg: {
    backgroundColor: Colors.closeButton,
    borderColor: Colors.closeButtonBorder,
    borderWidth: 1,
    alignItems: 'center',
    padding: 16,
    borderRadius: 3
  },
  closeText: {
    fontSize: Fonts.buttonFontSize,
    color: Colors.subText,
    fontFamily: Fonts.regular
  },
  retryBg: {
    backgroundColor: Colors.zalo,
    alignItems: 'center',
    paddingTop: 13,
    paddingBottom: 13,
    paddingLeft: 60,
    paddingRight: 60,
    borderRadius: 100
  }
  /*eslint-enable react-native/no-unused-styles */
});

const DefaultButton = props => (
  <View style={[styles.container, props.style]}>
    <TouchableHighlight
      {...props}
      style={[styles.buttonStyle, props.buttonStyle]}
    >
      <Text style={[styles.buttonText, props.textStyle]} numberOfLines={1}>
        {props.text}
      </Text>
    </TouchableHighlight>
  </View>
);
DefaultButton.propTypes = {
  ...TouchableHighlight.propTypes,
  text: PropTypes.string,
  buttonStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.number]),
  textStyle: PropTypes.number,
  style: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.number,
    PropTypes.object
  ])
};
DefaultButton.defaultProps = {
  underlayColor: Colors.subButton,
  buttonStyle: styles.defaultBg
};

const DetailButton = props => <DefaultButton {...props} />;
DetailButton.defaultProps = {
  underlayColor: Colors.subButtonTapped,
  buttonStyle: styles.detailBg
};

const CloseButton = props => <DefaultButton {...props} />;
CloseButton.defaultProps = {
  underlayColor: Colors.closeButtonTapped,
  buttonStyle: styles.closeBg,
  textStyle: styles.closeText
};

const RetryButton = props => <DefaultButton {...props} />;
RetryButton.defaultProps = {
  buttonStyle: styles.retryBg
};
class ZPButton extends React.Component {
  components = {
    [ZPButtonType.Detail]: DetailButton,
    [ZPButtonType.Close]: CloseButton,
    [ZPButtonType.Retry]: RetryButton,
    [ZPButtonType.Default]: DefaultButton
  };
  render() {
    const Component = this.components[
      this.props.buttonType || ZPButtonType.Default
    ];
    return <Component {...this.props} />;
  }
}
ZPButton.propTypes = {
  buttonType: PropTypes.number
};

export { ZPButton, DefaultButton, DetailButton, CloseButton, RetryButton };
