import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Dimensions, Text } from 'react-native';

import { Colors, Fonts } from '../themes';
import ZPIcon from '../icomoon';

const dialogWidth = 100;
const dialogHeight = 100;
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    width: dialogWidth,
    height: dialogHeight,
    left: window.width / 2 - dialogWidth / 2,
    top: window.width / 2 - dialogHeight / 2
  },
  opacity: {
    position: 'absolute',
    opacity: 0.54,
    backgroundColor: Colors.black,
    borderRadius: 8,
    width: dialogWidth,
    height: dialogHeight
  },
  text: {
    fontFamily: Fonts.regular,
    fontSize: Fonts.textSize,
    color: Colors.white,
    backgroundColor: Colors.transparent,
    textAlign: 'center'
  }
});

let timeout = undefined;

class ToastView extends React.Component {
  constructor(props) {
    super(props);

    this._handleShowToast = this._handleShowToast.bind(this);

    this.state = {
      showToast: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.showToast !== nextProps.showToast) {
      this.setState({ showToast: nextProps.showToast });
    }
  }

  componentWillUnmount() {
    this._clearToast();
  }

  _handleShowToast() {
    if (timeout) {
      return;
    }

    timeout = setTimeout(() => {
      this.props.onShowToast && this.props.onShowToast(false);
      this.setState({ showToast: false });
      this._clearToast();
    }, 2000);
  }

  _setStyle() {
    const { width, height, screenWidthRatio, screenHeightRatio } = this.props;
    let containerStyle = {};
    let dialogStyle = {};
    if (width && height) {
      const leftPos = window.width * (screenWidthRatio || 0.5) - width / 2;
      const topPos = window.height * (screenHeightRatio || 0.5) - height / 2;
      containerStyle = { width, height, left: leftPos, top: topPos };
      dialogStyle = { width, height };
    }
    return { containerStyle, dialogStyle };
  }

  _clearToast = function() {
    if (timeout) {
      clearTimeout(timeout);
      timeout = undefined;
    }
  };

  render() {
    if (!this.state.showToast) {
      return <View />;
    }

    const styleSheet = this._setStyle();
    this._handleShowToast();
    return (
      <View style={[styles.container, styleSheet.containerStyle]}>
        <View style={[styles.opacity, styleSheet.dialogStyle]} />
        <ZPIcon name={'general_check'} size={35} color={Colors.white} />
        <Text style={styles.text}>{this.props.text}</Text>
      </View>
    );
  }
}

ToastView.propTypes = {
  showToast: PropTypes.bool,
  onShowToast: PropTypes.func,
  text: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  screenWidthRatio: PropTypes.number,
  screenHeightRatio: PropTypes.number
};

export default ToastView;
