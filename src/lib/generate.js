import { NativeModules } from 'react-native';

export function setZaloPayApi(zaloPayApi) {
  window.ZaloPayApi = zaloPayApi;
}
export function getZaloPayApi() {
  if (!window.ZaloPayApi) {
    return NativeModules.ZaloPay;
  }
  return window.ZaloPayApi;
}
