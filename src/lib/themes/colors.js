const white = '#ffffff';
const black = '#000000';
const transparent = 'transparent';
const line = '#e3e6e7';
const defaultText = '#24272b';
const subText = '#727f8c';
const zalo = '#008fe5';
const pay = '#06be04';
const error = '#ff3a30';
const placeholder = '#acb3ba';
const defaultBackground = '#F0F4F7';
const avatarHolder = '#C9D4E5';
const maintainTitle = '#d49c05';
const netStatusBackground = '#ffffc1';
const noConnection = '#585858';
const checkConnection = '#cd7a04';
const redPacket = '#ea1d0e';
const lucky = '#f68a1f';

const promotion = '#ff7b0a';

const disableButton = '#c7c7cc';
const subButton = '#4abbff';
const subButtonTapped = '#91D6FF';
const closeButtonBorder = '#4abbff';
const closeButton = '#e5f5ff';
const closeButtonTapped = '#F5FCFF';

function hexToRgb(hex, alpha = 1) {
  hex = hex.replace(/[^0-9A-F]/gi, '');
  const bigint = parseInt(hex, 16);
  const r = (bigint >> 16) & 255;
  const g = (bigint >> 8) & 255;
  const b = bigint & 255;
  return [r, g, b, alpha];
}

export default {
  white,
  black,
  transparent,
  zalo,
  pay,
  line,
  defaultText,
  subText,
  error,
  placeholder,
  redPacket,
  defaultBackground,
  disableButton,
  subButtonTapped,
  lucky,
  hexToRgb,
  subButton,
  closeButton,
  closeButtonBorder,
  closeButtonTapped,
  avatarHolder,
  maintainTitle,
  promotion,
  netStatusBackground,
  noConnection,
  checkConnection,
};
