import { Platform, PixelRatio } from 'react-native';

const regular = Platform.OS === 'ios' ? 'SFUIText-Regular' : 'Roboto-Regular';
const semibold = Platform.OS === 'ios' ? 'SFUIText-Semibold' : 'Roboto-Medium';
const medium = Platform.OS === 'ios' ? 'SFUIText-Medium' : 'Roboto-Medium';

const ratio = PixelRatio.get();

const headerSectionSize = 13;
const buttonFontSize = 18;

let navigationSize = 18;
let textSize = 16;
let smallTextSize = 15;
let noteSize = 13;
let subTextSize = 14;
let smallSubTextSize = 13;

if (Platform.OS === 'android' && ratio >= 3) {
  navigationSize = 20;
  textSize = 15;
  smallTextSize = 14;
  noteSize = 12;
  subTextSize = 13;
  smallSubTextSize = 12;
} else if (ratio < 3) {
  navigationSize = 16;
  textSize = 15;
  smallTextSize = 14;
  noteSize = 11;
  subTextSize = 13;
  smallSubTextSize = 12;
}

export default {
  regular,
  semibold,
  medium,

  buttonFontSize,
  navigationSize,
  textSize,
  smallTextSize,
  headerSectionSize,
  noteSize,
  subTextSize,
  smallSubTextSize
};
