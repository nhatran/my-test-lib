import React, { Component } from 'react';
import {
    Text,
    View,
    NetInfo,
    StyleSheet,
    TouchableWithoutFeedback,
} from 'react-native';

import { Colors, Fonts, getZaloPayApi } from '../generate';


const NO_CONNECTION = 'Không có kết nối mạng!';
const CHECK_CONNECTION = 'Kiểm tra kết nối';

const styles = StyleSheet.create({
    containerWrapper: {
        flex: 1,
        height: 40,
        padding: 12
    },
    container: {
        justifyContent: 'center',
        backgroundColor: Colors.netStatusBackground,
        alignItems: 'center',
        height: 40,
    },
    noConnection: {
        color: Colors.noConnection,
        fontFamily: Fonts.regular,
        fontSize: Fonts.subTextSize,
    },
    checkConnection: {
        color: Colors.checkConnection,
        fontFamily: Fonts.regular,
        fontSize: Fonts.subTextSize,
    }
});

export default class NetStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            networkStatus: true,
        };
        this.handleFirstConnectivityChange = this.handleFirstConnectivityChange.bind(this);
    }

    componentDidMount() {
        NetInfo.isConnected.fetch().then(isConnected => {
            this.setState({
                networkStatus: isConnected,
            });
        });
        NetInfo.isConnected.addEventListener('connectionChange', this.handleFirstConnectivityChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange
        );
    }

    handleFirstConnectivityChange(isConnected) {
        console.log('handleFirstConnectivityChange', isConnected);
        this.setState({ networkStatus: isConnected });
    }

    render() {
        const { networkStatus } = this.state;
        const NativeModule = getZaloPayApi();
        if (networkStatus) {
            return null;
        }
        return (
          <TouchableWithoutFeedback onPress={() => NativeModule.showNoInternetConnection()} style={styles.containerWrapper}>
            <View style={styles.container}>
              <Text style={styles.noConnection}>{NO_CONNECTION}
                {' '}
                <Text style={styles.checkConnection}>{CHECK_CONNECTION}</Text>
              </Text>
            </View>
          </TouchableWithoutFeedback>
        );
    }
}
