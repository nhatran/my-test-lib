import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { NativeModules, Platform, PixelRatio, processColor, Text, View } from 'react-native';

const NativeIconAPI = NativeModules && (NativeModules.RNVectorIconsManager || NativeModules.RNVectorIconsModule);

const DEFAULT_ICON_SIZE = 12;
const DEFAULT_ICON_COLOR = 'black';
const $transparent = 'transparent';

export default function createIconSet(glyphMap, colorMap, fontFamily, fontFile) {
  let fontReference = fontFamily;
  // Android doesn't care about actual fontFamily name, it will only look in fonts folder.
  if (Platform.OS === 'android' && fontFile) {
    fontReference = fontFile.replace(/\.(otf|ttf)$/, '');
  }

  const IconNamePropType = PropTypes.oneOf(Object.keys(glyphMap));

  class Icon extends Component {
    static propTypes = {
      name: IconNamePropType.isRequired,
      size: PropTypes.number,
      color: PropTypes.string,
      subColor: PropTypes.string,
      colors: PropTypes.arrayOf(PropTypes.string),
      style: PropTypes.number,
      iconStyle: PropTypes.number,
      children: PropTypes.node,
    };

    static defaultProps = {
      size: DEFAULT_ICON_SIZE,
      allowFontScaling: false,
    };

    setNativeProps(nativeProps) {
      if (this._root) {
        this._root.setNativeProps(nativeProps);
      }
    }
    render() {
      const { name, size, color, colors, style, iconStyle, ...props } = this.props;
      let glyphs = glyphMap[name] || '?';
      if (!Array.isArray(glyphs)) {
        glyphs = [glyphs];
      }
      if (colorMap[name]) {
        glyphs = glyphs.slice(0, colorMap[name].length);
      }
      glyphs = glyphs.map(_glyph => {
        if (typeof _glyph === 'number') {
          _glyph = String.fromCharCode(_glyph);
        }
        return _glyph;
      });
      let colorStyles = [];
      if (colorMap[name]) {
        colorMap[name].map(object => {
          const colorStyle = {
            color: object.fill,
          };
          colorStyles.push(colorStyle);
          return colorStyle;
        });
      }
      if (colors !== undefined && colors.length > 0) {
        colors.map((color, index) => {
          colorStyles[index] = {
            color,
          };
          return color;
        });
      }

      if (color) {
        colorStyles = [];
        glyphs.forEach(() => {
          colorStyles.push({ color });
        });
      }
      const styleDefaults = {
        fontSize: size,
        lineHeight: size,
        fontWeight: 'normal',
        fontStyle: 'normal',
        backgroundColor: $transparent,
      };
      const _subStyle = [
        styleDefaults,
        {
          position: 'absolute',
          top: 0,
          flex: 1,
        },
      ];
      props.ref = component => {
        this._root = component;
      };

      styleDefaults.fontFamily = fontReference;
      return (
        <View style={style}>
          {glyphs.map((glyph, index) => {
            if (index === 0) {
              return (
                <Text allowFontScaling={false} style={[styleDefaults, colorStyles[index], iconStyle]} {...props} key={index}>
                  {glyph}
                </Text>
              );
            }
            return (
              <Text allowFontScaling={false} style={[_subStyle, colorStyles[index], iconStyle]} {...props} key={index}>
                {glyph}
              </Text>
            );
          })}
        </View>
      );
    }
  }

  const imageSourceCache = {};

  function getImageSource(name, size = DEFAULT_ICON_SIZE, color = DEFAULT_ICON_COLOR) {
    if (!NativeIconAPI) {
      if (Platform.OS === 'android') {
        throw new Error('RNVectorIconsModule not available, did you properly integrate the module?');
      }
      throw new Error('RNVectorIconsManager not available, did you add the library to your project and link with libRNVectorIcons.a?');
    }

    let glyph = glyphMap[name] || '?';

    if (typeof glyph === 'number') {
      glyph = String.fromCharCode(glyph);
    }

    const proessedColor = processColor(color);
    const cacheKey = glyph + ':' + size + ':' + proessedColor;
    const scale = PixelRatio.get();

    return new Promise((resolve, reject) => {
      const cached = imageSourceCache[cacheKey];
      if (typeof cached !== 'undefined') {
        if (!cached || cached instanceof Error) {
          reject(cached);
        }
        resolve({ uri: cached, scale });
      } else {
        NativeIconAPI.getImageForFont(fontReference, glyph, size, proessedColor, (err, image) => {
          const error = typeof err === 'string' ? new Error(err) : err;
          imageSourceCache[cacheKey] = image || error || false;
          if (!error && image) {
            resolve({ uri: image, scale });
          } else {
            reject(error);
          }
        });
      }
    });
  }
  Icon.getImageSource = getImageSource;
  return Icon;
}
