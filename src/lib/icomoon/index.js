import createIcon from './create-icon';
import zaloPayConfig from '../fonts/zalopay.json';

const Icon = createIcon(zaloPayConfig, 'zalopay');
export default Icon;
