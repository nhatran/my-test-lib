import { NativeModules, Platform } from 'react-native';

import RegisterAsset from 'react-native/Libraries/Image/AssetRegistry';
import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';
import {
  getZaloPayApi
} from '../generate';

const { SourceCode } = NativeModules;
const loaded = {};
const loading = {};
const onLoadPromises = {};

export function isLoaded(name) {
  return !!loaded[name];
}

export async function loadAsync(nameOrMap) {
  if (typeof nameOrMap === 'object') {
    await Promise.all(nameOrMap.map(name => loadAsync(name)));
    return;
  }

  const name = nameOrMap;
  if (loaded[name]) {
    return;
  } else if (loading[name]) {
    await new Promise(resolve => {
      onLoadPromises[name].push(resolve);
    });
  } else {
    loading[name] = true;
    onLoadPromises[name] = [];

    const uri = await getFontUri(name);
    await downloadAndLoadFontAsync(name, uri);
    loaded[name] = true;
    delete loading[name];
    if (onLoadPromises[name]) {
      onLoadPromises[name].forEach(resolve => resolve());
      delete onLoadPromises[name];
    }
  }
}

function getFontUri(name) {
  console.log('getFontUri NAME', name);
  let httpServerLocation;
  if (__DEV__) {
    httpServerLocation = '/assets/fonts';
  } else {
    httpServerLocation = '/fonts';
    if (Platform.OS === 'android') {
      const path = 'file://' + getBundleSourcePath() + 'fonts/' + name + '.ttf';
      console.log('getFontUri PATH', path);
      return path;
    }
  }
  const resRegister = {
    __packager_asset: true,
    httpServerLocation,
    type: 'ttf',
    scales: [1],
    name
  };
  console.log('getFontUri RESOURCE', resRegister);
  const sourceId = RegisterAsset.registerAsset(resRegister);
  const asset = resolveAssetSource(sourceId);
  console.log('getFontUri ASSET', asset);
  return asset.uri;
}

function getBundleSourcePath() {
  const scriptURL = SourceCode.scriptURL;
  if (scriptURL.startsWith('assets://')) {
    return null;
  }
  if (scriptURL.startsWith('file://')) {
    return scriptURL.substring(7, scriptURL.lastIndexOf('/') + 1);
  }

  return scriptURL.substring(0, scriptURL.lastIndexOf('/') + 1);
}

async function downloadAndLoadFontAsync(name, uri) {
  const ZaloPayApi = getZaloPayApi();
  if (ZaloPayApi && ZaloPayApi.loadFontAsync) {
    await ZaloPayApi.loadFontAsync(name, uri);
  }
  return true;
}

export default { loadAsync };
