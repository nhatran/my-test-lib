import createIconSet from './create-icon-set';

function createIcon(config, fontFamilyArg, fontFile) {
  const glyphMap = {};
  const colorMap = {};
  const fontFamily = fontFamilyArg;
  for (const name in config) {
    const object = config[name];
    if (object.codes !== undefined && object.codes.length > 1) {
      glyphMap[name] = object.codes;
    } else {
      glyphMap[name] = object.code;
    }
    colorMap[name] = object.colors;
  }
  return createIconSet(glyphMap, colorMap, fontFamily, fontFile || fontFamily + '.ttf');
}

export default createIcon;
